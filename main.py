# *- coding: utf-8 -*-
"""This is the main project file. The program is started using this file."""


import ctypes
import os
import sys
import PyQt5

from PyQt5 import QtWidgets, QtGui

import Source.Resources_rc
from Source.cls_database import DataBaseManager
from Source.cls_MainWindow import MainWindow


def main():
    """Функция старта программы."""

    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(":/app/icon.ico"))
    app.setApplicationDisplayName("Memes Lab")

    database = DataBaseManager()

    window = MainWindow()
    window.show()

    sys.exit(app.exec())


if __name__ == "__main__":
    # Для адекватного отображения значка приложения
    application_id = u'SibFU.MemesLab.MemesLab.v0.1'
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(application_id)

    pyqt = os.path.dirname(PyQt5.__file__)
    QtWidgets.QApplication.addLibraryPath(os.path.join(pyqt, "Qt", "plugins"))

    if not os.path.exists(".\\Media"):
        os.mkdir(".\\Media")
    main()
