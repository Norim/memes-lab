# *- coding: utf-8 -*-
"""Функция создания миниатюры изображения. Разрещение 200x200px."""

import os
from PIL import Image

RESOLUTION = 200
SAVE_FOLDER = '.\\Media\\Image\\tmp\\'


def create_thumbnail_image(image_path: str, image_name: str):
    """
    Создает миниатюру изображения в папке SAVE_FOLDER
    :param image_path: путь к изображению
    :param image_name: имя для миниатюры
    :return: путь к созданной миниатюре
    """
    try:
        image = Image.open(image_path)
    except:
        return None

    width, height = image.size

    max_size = max(width, height)
    min_size = min(width, height)
    coeff = RESOLUTION / max_size
    min_size *= coeff

    if max_size == width:
        thumbnail_image_size = (RESOLUTION, min_size)
    elif max_size == height:
        thumbnail_image_size = (min_size, RESOLUTION)
    else:
        return None

    if not os.path.exists(SAVE_FOLDER):
        os.mkdir(SAVE_FOLDER)

    image.thumbnail(thumbnail_image_size, Image.ANTIALIAS)

    save_path = SAVE_FOLDER + image_name
    image.save(save_path)
    return save_path


if __name__ == "__main__":
    SAVE_FOLDER = '..\\Media\\Image\\tmp\\'
    print(create_thumbnail_image("..\\Media\\Image\\IMG_20200415_191036_1.jpg", "IMG_20200415_191036_1.jpg"))
