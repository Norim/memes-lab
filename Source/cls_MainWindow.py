# *- coding: utf-8 -*-
"""Основное окно программы."""
import random

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QApplication, QSystemTrayIcon, QAction, qApp, QMenu, QDesktopWidget
from PyQt5.QtGui import QIcon, QImage
from PyQt5.QtCore import QMimeData

from Source.cls_database import DataBaseManager
from Source.cls_Setting import Setting
from Source.cls_FileManager import FileManager
from Source.cls_mempaint import MemPaint
from Source.cls_DropWidget import DropWidget
from Source.UI_MainWindow import Ui_MemesLab
from Source.cls_ContentForm import ContentForm
from Source.QCastomWindow import QRoundWindow


class MainWindow(QRoundWindow, Ui_MemesLab):
    """Класс основного окна программы."""

    def __new__(cls):
        """Класс является синглетоном."""
        if not hasattr(cls, 'instance'):
            cls.instance = super(MainWindow, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        """Инициализация класса."""
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle(None)

        self.database = DataBaseManager()

        self.setting = Setting(self)
        self.file_manager = FileManager(self)
        self.dnd_widget = DropWidget(self)
        self.redactor = MemPaint(self)

        self.id_list = []

        # a = DataBaseManager()
        # ids = a.get_all_IDs()
        # for i in ids:
        #     print(a.get_information_by_ID(i))

        self.Settings_Button.clicked.connect(self.setting.show)

        self.tray_icon = QSystemTrayIcon(QIcon(":/app/icon.ico"))
        self.tray_window_show = False
        self.tray_icon.activated.connect(self.restore_window)
        quit_action = QAction("Exit", self)
        quit_action.triggered.connect(qApp.quit)
        setting_action = QAction("Setting", self)
        setting_action.triggered.connect(self.setting.show)

        tray_menu = QMenu()
        tray_menu.addAction(setting_action)
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)

        self.__center()
        self.set_complitor()
        self.number_of_collum = 3;
        self.widgets_collum = []
        self.last_id = None;
        self.is_Resize = False;

        self.Main.setCurrentIndex(0)  # Открываем на старте домашнюю область

        # self.mem = namedtuple("Mem", "id name description path_file path_miniature tags")  # тестовое
        # mem = self.mem(1234, "Holaghdhghghgf gdghgh  thrh dgf", "Hello", "/Data/Interface/Kanna_Big.png",
        #                "/Data/Interface/Kanna_Big.png",
        #                ["Дед", "Медвед", "Картон", "Обед", "Не", "Бейте", "Пожалуйста"])

        # Главное окно
        self.Home_Button.clicked.connect(lambda: self.Main.setCurrentIndex(0))
        self.Add_Button.clicked.connect(lambda: self.Main.setCurrentIndex(2))
        self.List_Button.clicked.connect(lambda: self.Main.setCurrentIndex(1))
        self.Time_Lapse_Button.clicked.connect(self.time_lapse_list)
        self.Differed_Button.clicked.connect(self.differed_list)
        self.Search_String.editingFinished.connect(self.Search_Button.animateClick)
        self.Search_Button.clicked.connect(self.search_view)
        self.Search_Button.clicked.connect(self.Search_String.clear)
        self.Mixer.clicked.connect(self.random_id_list)
        self.Popular_Sorted.clicked.connect(self.copy_number_id_list)
        self.Old_Sorted.clicked.connect(self.old_time_id_list)
        self.New_Sorted.clicked.connect(self.new_time_id_list)
        # Домашняя область:
        # self.From_Folder.clicked.connect(self.new_time_id_list)
        # Редактор:
        self.Meme_Tag_String.editingFinished.connect(self.Meme_Add_Tag.animateClick)
        self.Meme_Add_Tag.clicked.connect(self.add_tag_to_meme)
        self.Meme_Add_Tag.clicked.connect(self.Meme_Tag_String.clear)
        self.Save_Button.clicked.connect(self.save_file_in_bd)
        self.Delete_Button.clicked.connect(self.delete_file_in_bd)
        self.Meme_Redacting.currentIndexChanged.connect(self.meme_redacting)
        self.Change_Picture.clicked.connect(self.redactor_Picture)
        self.Copy_Button.clicked.connect(self.copy_redactor_picture)

        self.From_Folder.clicked.connect(self.file_manager.explorer)

        self.redactor.closed.connect(self.update_redactor_Picture)

        self.refresh()
        self.setting.update_styles()

    def refresh(self):
        """Метод обновления состояния окна."""
        if self.setting.allow_tray:
            self.tray_icon.show()
        else:
            self.tray_icon.hide()

    def call_redactor(self, path: str):
        """
        Метод вызова окна редактирования изображения.
        :param path: путь к изображению
        :return: None
        """
        self.redactor.set_image(path)
        self.redactor.show()
        self.redactor.activateWindow()

    def result_redactor(self):
        """Метод получения отредактированного файла из редактора."""
        return self.redactor.last_save

    def set_complitor(self):
        """Метод обновления подсказок тегов."""
        search_list = self.database.get_all_tags()
        for i in range(len(search_list)):
            search_list[i] = "#" + search_list[i]
        self.Search_String.Compliter(search_list)
        group_list = self.database.get_all_Groups()
        group_list.insert(0, "all")
        # print(group_list)
        if "" in group_list: group_list.remove("")
        for i in range(len(group_list)):
            group_list[i] = group_list[i].capitalize()
        # print(group_list)
        self.Group_Select.clear()
        self.Group_Select.addItems(group_list)
        format_list = ["Meme"]
        self.Format_Select.clear()
        self.Format_Select.addItems(format_list)

    def clipboard(self, path):
        data = QMimeData()
        data.setImageData(QImage(path))
        clip = QApplication.clipboard()
        clip.setMimeData(data)

    def __center(self):
        """Метод для установки окна в центре экрана."""
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    # --------------------------Галлерея--------------------------------------------

    def search_view(self, tag=None):
        find_description = None
        find_group = None
        find_format = None
        find_tags = None
        if type(tag) is str:
            find_tags = tag
        self.List_Title.setText("#all")
        find_words = self.Search_String.text()
        if (find_tags is None):
            if (find_words == ""):
                find_tags = "all"
            else:
                if ("#" in find_words):
                    find_tags = find_words.split("#")
                    if "" in find_tags: find_tags.remove("")
                    for i in range(len(find_tags)):
                        find_tags[i] = find_tags[i].strip(' ,;').lower()
                    text = "#" + '; #'.join(find_tags)
                    self.List_Title.setText(text)
                else:
                    find_description = find_words.lower()
                    self.List_Title.setText(find_description)
                if self.Group_Select.currentText() != "All":
                    find_group = self.Group_Select.currentText().lower()
                # if self.Format_Select.currentText() != "Nope":
                #   find_format = select.Format_Select.currentText().lower() #Раскомитить для работы поиска по форматам
                # print(find_tags)
                # print(find_description)
                # print(find_group)
                # print(find_format)
        self.id_list = self.database.search(tags=find_tags, description=find_description,
                                            group=find_group, f_format=find_format)
        self.set_gallery()

    def random_id_list(self):
        random.shuffle(self.id_list)
        if not self.id_list is None:
            self.set_gallery()

    def new_time_id_list(self):
        self.id_list.sort(key=lambda id: self.database.get_information_by_Id(id).DataTime, reverse=True)
        if not self.id_list is None:
            self.set_gallery()

    def old_time_id_list(self):
        self.id_list.sort(key=lambda id: self.database.get_information_by_Id(id).DataTime)
        if not self.id_list is None:
            self.set_gallery()

    def copy_number_id_list(self):
        self.id_list.sort(key=lambda id: self.database.get_information_by_Id(id).CopyNumber, reverse=True)
        if not self.id_list is None:
            self.set_gallery()

    def time_lapse_list(self):
        self.id_list = self.database.get_all_Id(100)
        self.List_Title.setText("Time-Lapse")
        self.new_time_id_list()

    def differed_list(self):
        self.id_list = self.database.get_all_Id_with_no_tag()
        self.List_Title.setText("Отложенные")
        self.new_time_id_list()

    def clearLayout(self, layout):
        # print(layout.count())
        while layout.count():
            child = layout.takeAt(0)
            # print(type(child))
            if child.layout():
                self.clearLayout(child)
                child.layout().deleteLater()
            if child.widget():
                child.widget().deleteLater()
        # print(layout.count())

    def set_gallery(self, id_list=None):
        self.clearLayout(self.Gallery_Layout)

        if not self.id_list is None:
            self.Main.setCurrentIndex(1)

        id_list = id_list or self.id_list
        # print(id_list)

        # Лайаут галереи - self.Gallery_Layout

        max_collum = (self.width() - self._Menu_Wiget.width() - 10) // (ContentForm.maxWidth + 10)
        list_of_layout = []
        # Создаём колонки
        for i in range(max_collum):
            layout = QtWidgets.QFormLayout()
            layout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
            layout.setLabelAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
            layout.setFormAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
            self.Gallery_Layout.addLayout(layout)
            list_of_layout.append(layout)
        # Заполняем колонки
        for i in range(len(id_list)):
            parent = self.Desk_ContentForm
            object_namedtuple = self.database.get_information_by_Id(id_list[i])
            picture_format = ContentForm(parent, object_namedtuple)
            picture_format.open_info.connect(self.open_info_by_emit)
            picture_format.search.connect(self.search_by_emit)
            picture_format.coppy_buffer.connect(self.coppy_buffer_by_emit)
            list_of_layout[i % len(list_of_layout)].setWidget(i / len(list_of_layout), QtWidgets.QFormLayout.LabelRole,
                                                              picture_format)
        self.update()

    # --------------------------Редактирование/Информация--------------------------------------------
    def meme_redacting(self):
        if self.Meme_Redacting.currentIndex() == 1:
            self.Meme_Title.setReadOnly(True)
            self.Meme_Description_String.setReadOnly(True)
            self.Meme_Group_String.setReadOnly(True)
            self.Meme_Tag_String.setReadOnly(True)
            self.Meme_scrollArea.setReadOnly(True)
        if self.Meme_Redacting.currentIndex() == 0:
            self.Meme_Title.setReadOnly(False)
            self.Meme_Description_String.setReadOnly(False)
            self.Meme_Group_String.setReadOnly(False)
            self.Meme_Tag_String.setReadOnly(False)
            self.Meme_scrollArea.setReadOnly(False)

    def open_info(self, id):
        self.last_id = id
        self.Meme_Redacting.setCurrentIndex(1)
        self.Main.setCurrentIndex(2)
        object_namedtuple = self.database.get_information_by_Id(id)

        self.Meme_Title.clear()
        if object_namedtuple.Title != "Untitled":
            self.Meme_Title.setText(object_namedtuple.Title.capitalize())
        self.Meme_Description_String.clear()
        if object_namedtuple.Description != "NONE":
            self.Meme_Description_String.setText(object_namedtuple.Description.capitalize())
        self.Meme_Group_String.clear()
        if object_namedtuple.Group != "Nope":
            self.Meme_Group_String.setText(object_namedtuple.Group.capitalize())
        self.Meme_Picture.clear()
        if object_namedtuple.TIPath != "None":
            # width = self.Meme_Picture_Bar.height() * (picture.width() / picture.height())
            # self.Meme_Picture.setMinimumSize(QtCore.QSize(width, self.Meme_Picture_Bar.height()))
            self.Meme_Picture.setPicture(object_namedtuple.FPath)
        self.Meme_scrollArea.clear()
        if object_namedtuple.Tags[0] != "Nope":
            self.Meme_scrollArea.add_tags(object_namedtuple.Tags, 1)
        tag = self.database.get_all_tags()
        self.Meme_Tag_String.Compliter(tag)

        group = self.database.get_all_Groups()
        for i in range(len(group)):
            group[i] = group[i].capitalize()
        self.Meme_Group_String.Compliter(group)

    def add_tag_to_meme(self):
        tags = self.Meme_scrollArea.get_all_tags()
        tag = self.Meme_Tag_String.text().lower()
        if tag == "": return
        if "#" in tag: return
        if " " in tag: tag = tag.split(" ")[0]
        if not tag in tags:
            tags.append(tag)
        self.Meme_scrollArea.add_tags(tags, 1)

    def redactor_Picture(self):
        path = self.Meme_Picture.getPath()
        if path:
            self.call_redactor(path)

    def update_redactor_Picture(self):
        path = self.Meme_Picture.getPath()
        new_path = self.result_redactor()
        if new_path != path:
            self.last_id = self.database.get_Id_by_tags("#new#")[
                0]  # Для функции save_file_in_bd, чтобы создать новую запись в БД
            self.database.update_tags_by_Id(self.last_id)
            self.open_info(self.last_id)

    def save_file_in_bd(self):
        last_id = self.last_id
        if last_id:
            object_namedtuple = self.database.get_information_by_Id(self.last_id)
            # print("---------")
            # print(object_namedtuple)
            Title = self.Meme_Title.text().lower()
            self.database.update_Title_by_Id(last_id, Title)
            Description = self.Meme_Description_String.toPlainText()
            # print(Description)
            self.database.update_Description_by_Id(last_id, Description)
            Group = self.Meme_Group_String.text().lower()
            self.database.update_Group_by_Id(last_id, Group)
            image_path = object_namedtuple.FPath
            if image_path == "None" and (image_path == "*.png" or image_path == "*.jpg"):
                image_name = f"#{object_namedtuple.DateTime}.png"
                TIPath = create_thumbnail_image(image_path, image_name)
                self.database.update_TIPath_by_Id(last_id, TIPath)
            Tags = self.Meme_scrollArea.get_all_tags()
            if not "all" in Tags: Tags.append("all")
            # print(Tags)
            self.database.update_tags_by_Id(last_id, Tags)
            object_namedtuple = self.database.get_information_by_Id(self.last_id)
            # print(object_namedtuple)
            self.last_id = None
            self.update_search_list()
            self.set_complitor()

    def delete_file_in_bd(self):
        self.database.del_file_by_Id(self.last_id)
        self.last_id = None
        self.search_view()

    def copy_redactor_picture(self):
        path = self.Meme_Picture.path
        self.clipboard(path)

    def update_search_list(self):
        self.Main.setCurrentIndex(1)
        self.set_gallery()

    # def save_info:
    # def wiew_list(self):
    #
    #
    #     self.widgets_collum = []
    #     for i in range(self.number_of_collum):
    #         self.widgets_collum.add(QtWidgets.QWidget(self.Desk_ContentForm))
    #         sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
    #         sizePolicy.setHorizontalStretch(0)
    #         sizePolicy.setVerticalStretch(0)
    #         sizePolicy.setHeightForWidth(self.widgets_collum[i].sizePolicy().hasHeightForWidth())
    #         self.widgets_collum[i].setSizePolicy(sizePolicy)
    #         self.widgets_collum[i].setMinimumSize(QtCore.QSize(350, 0))
    #         self.widgets_collum[i].setObjectName("widgets_collum[i]")
    #         self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.widgets_collum[i])
    #         self.verticalLayout_6.setObjectName("verticalLayout_6")
    #         self.horizontalLayout_5.addWidget(self.widgets_collum[i])
    #
    # def set_widget_collum(self, i):
    #

    def restore_window(self, reason):
        """Метод для восстановления пока по клику иконки в трее."""
        if reason == QSystemTrayIcon.DoubleClick:
            if self.tray_window_show:
                self.hide()
                self.tray_window_show = False
            else:
                self.show()
                self.showNormal()
                self.tray_window_show = True

    def closeEvent(self, event):
        """Переопределение события закрытия окна."""
        if self.setting.allow_tray:
            event.ignore()
            self.hide()
            self.tray_window_show = False
        else:
            self.setting.close()
            self.dnd_widget.close()
            self.redactor.close()
            event.accept()

    def resizeEvent(self, event):
        self.is_Resize = True

    def mouseReleaseEvent(self, event):
        if self.is_Resize:
            self.set_gallery()
            self.is_Resize = False

    # ------------------Обработка сигналов из встроенных виджетов--------------------

    def open_info_by_emit(self):
        id = self.sender().object_namedtuple.Id
        self.open_info(id)

    def search_by_emit(self):
        search_tag = self.sender().search_tag
        self.search_view(search_tag)

    def coppy_buffer_by_emit(self):
        path_full_picture = self.sender().object_namedtuple.FPath
        id = self.sender().object_namedtuple.Id
        self.clipboard(path_full_picture)
        self.database.increase_CopyNumber_by_Id(id)

    # def customEvent(self, event):
    #     if event.type() == OpenFileInfoEvent.type():
    #         self.open_info(event.id)
