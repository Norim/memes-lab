# *- coding: utf-8 -*-
"""Менеджер для работы с локальными и сетевыми файлами."""

import os
import time
import copy
import urllib.request

from PyQt5.QtCore import QThread, QBasicTimer, QUrl
from PyQt5.QtWidgets import QFileDialog, QWidget

import Source.fun_extension as exten
from Source.fun_ThumbnailCreator import create_thumbnail_image


class _DMThread(QThread):
    """Класс потока QtCore.QThread для распараллеливания одновременных операций с файлами."""

    def __init__(self, caller, lst_local, lst_http):
        """
        Инициализация потока.
        :param caller: ссылка на объект, в котором создается поток
        :param lst_local: список ссылок на локальные файлы
        :param lst_http: список ссылок на сетевые файлы (http)
        """
        super().__init__()
        self.caller = caller
        self.lst_local = lst_local
        self.lst_http = lst_http

    def run(self):
        """Дейсвтивия выполняемые в потоке."""
        self.caller.save_local(self.lst_local)
        self.caller.save_http(self.lst_http)
        self.caller.thread_list.remove(self)


class FileManager(QWidget):
    """Класс файлового менеджера."""

    def __init__(self, parent=None):
        """Инициализация класса."""
        super().__init__()
        self.parent = parent

        self.t = ""
        self.cell = 0

        self.browser = ["http", "https"]
        self.url_folder = {"image": "Image", "video": "Video", "audio": "Audio", "text": "HTML"}
        self.thread_list = []
        self.buffer = []
        self.timer = QBasicTimer()
        self.timer.start(1000, self)

    @property
    def db(self):
        return self.parent.database

    def timerEvent(self, e):
        """Переопределение события по таймеру."""
        if len(self.buffer) != 0:
            self.__call_db()

    def explorer(self):
        title = ""
        path = "\\"
        formats = "Изображения (*.png *.jpg *.bmp)"
        file_list = QFileDialog.getOpenFileNames(self, title, path, formats)[0]
        self.t = time.strftime("%Y%m%d_%H%M%S", time.localtime())
        DMT = _DMThread(self, file_list, [])
        self.thread_list.append(DMT)
        DMT.start()

    def parsing_text(self, text: str):
        """
        Разбор переданного текста и поиск URL ссылок на файлы
        :param text: str() - текст для поиска
        :return: None
        """
        text = text.replace("\n", " ").replace("\t", " ").replace("\"", " ").replace("\'", " ").replace("\\", "/")
        text = text.replace("  ", " ").replace("   ", " ").replace("    ", " ")

        lst = text.split(" ")
        new_lst = []
        for i in lst:
            if "/" in i:
                uri = QUrl(i)
                new_lst.append(uri)
        self.parsing_url(new_lst)

    def parsing_url(self, url: list):
        """
        Разбор переданного списка URL ссылок и определение типа ссылок (локальные/сетевые)
        :param url: list() - список ссылок QtCore.QUrl
        :return: None
        """
        # Check local urls
        self.t = time.strftime("%Y%m%d_%H%M%S", time.localtime())

        local_files = []
        http_files = []
        for i in url:
            if i.isLocalFile() and i.isValid():
                x = i.toLocalFile()
                if os.path.isfile(x):
                    local_files.append(x)
            elif i.isValid():
                head = i.scheme()
                if head in self.browser:
                    http_files.append(i.toString())

        DMT = _DMThread(self, local_files, http_files)
        self.thread_list.append(DMT)
        DMT.start()

    def save_http(self, files: list):
        """
        Загрузка и сохраенние файлов по ссылкам http
        :param files: list() - список строк http адресов
        :return: None
        """
        for file in files:
            self.cell += 1

            try:
                req = urllib.request.urlopen(file)
                ver = 1
            except:
                ver = 0

            if ver == 1:
                if req.code == 200:
                    content = req.headers['content-type']
                    content = content.split(";")[0].split("/")

                    folder = "Other"
                    if content[0] == "application":
                        if content[1] in ["pdf", "xml", "msword"]:
                            folder = "Documents"
                        elif content[1] == "zip":
                            folder = "Archives"
                    else:
                        folder = self.url_folder[content[0]]

                    if content[0] == "image":
                        prefix = "IMG"
                    else:
                        prefix = content[1].upper()
                    data = req.read()
                    ext = "." + content[1]
                    file_path = f"Media\\{folder}\\{prefix}_{self.t}_{self.cell}{ext}"

                    if not os.path.exists(f"Media\\{folder}\\"):
                        os.mkdir(f".\\Media\\{folder}\\")

                    with open(file_path, "wb") as f:
                        f.write(data)
                    if prefix == "IMG":
                        create_thumbnail_image(file_path, f"{prefix}_{self.t}_{self.cell}{ext}")
                    self.__buffer_add(self.t, file_path, content[1], "None")
        self.cell = 0

    def save_local(self, files: list):
        """
        Сохранение локальных файлов.
        :param files: list() - список строк-абсолютных путей до локальных файлов
        :return: None
        """
        for file in files:
            self.cell += 1
            p = self.__create_path(file)
            ext = exten.getExt(p)

            with open(file, "rb") as f:
                copyf = f.read()

            with open(p, "wb") as f:
                f.write(copyf)

            if exten.isImage(file):
                create_thumbnail_image(p, f"IMG_{self.t}_{self.cell}.{ext}")

            self.__buffer_add(self.t, p, ext, "None")

        self.cell = 0

    def __call_db(self):
        """Добавление сохраненных файлов в базу данных. \nСвязанные классы: DataBaseManager"""
        # print(100)
        lst = copy.deepcopy(self.buffer)
        for i in lst:
            # print(i)
            # 0 - time
            # 1 - path
            # 2 - ext
            # 3 - description
            # 4 - thump - tipath
            self.db.add_file("Untitled", i[0], i[1], i[2], description=i[3], ti_path=i[4])
            self.buffer.remove(i)
        # print(200)

    def __buffer_add(self, arg1, arg2, arg3, arg4):
        """
        Добавление сохраненного файла в буфер для дальнейшего добавления данных в БД
        :param arg1: время формата YYMMDD_hhmmss
        :param arg2: путь к файлу
        :param arg3: расширение файла (без точки)
        :param arg4: описание файла
        :return:
        """
        # print(arg2)
        ti = arg2.split("\\")
        line = None
        if ti[1] == "Image":
            line = f"{ti[0]}\\{ti[1]}\\tmp\\{ti[2]}"

        lst = [arg1, arg2, arg3, arg4, line]
        self.buffer.append(lst)

    def __create_path(self, file: str):
        """
        Метод создания пути для копии локального файла и в зависимости от его типа распределение по подпапкам
        :param file: путь к файлу
        :return: путь для сохранения нового файла
        """
        ext = exten.getExt(file)

        if exten.isNoneType(file):
            ext = ""
            prefix = "NONE"
        else:
            prefix = ext.upper()
            ext = "." + ext

        if exten.isImage(file):
            prefix = "IMG"
            folder = "Image"
        elif exten.isDocument(file):
            folder = "Documents"
        elif exten.isAudio(file):
            folder = "Audio"
        elif exten.isVideo(file):
            folder = "Video"
        elif exten.isBook(file):
            folder = "Book"
        elif exten.isArchive(file):
            folder = "Archives"
        else:
            folder = "Other"

        new_path = f"Media\\{folder}\\{prefix}_{self.t}_{self.cell}{ext}"
        if not os.path.exists(f"Media\\{folder}\\"):
            os.mkdir(f".\\Media\\{folder}\\")
        return new_path
