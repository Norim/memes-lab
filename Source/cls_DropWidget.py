# *- coding: utf-8 -*-
"""Виджет программы для приема перетаскиваемых файлов."""

from PyQt5.QtCore import Qt, QBasicTimer
from PyQt5.QtGui import QCursor, QPixmap
from PyQt5.QtWidgets import QApplication, QLabel, QWidget


class DropWidget(QWidget):
    """Класс окна виджета"""

    def __new__(cls, parent=None):
        """Класс явялется синглетоном."""
        if not hasattr(cls, 'instance'):
            cls.instance = super(DropWidget, cls).__new__(cls)
        return cls.instance

    def __init__(self, parent=None):
        """Инициализация окна."""
        super().__init__()
        self.parent = parent

        self.wh = 128
        self.a = int(0.147 * self.wh)
        self.__setupUI()

        self.track = True
        self.activeDrag = False
        self.count = 0

        self.refresh()

    @property
    def file_manager(self):
        return self.parent.file_manager

    @property
    def setting(self):
        return self.parent.setting

    def refresh(self):
        """Метод для обновления состояния и поведения окна."""
        if self.setting.allow_widget:
            self.timer.start(50, self)
        else:
            self.timer.stop()

    def timerEvent(self, e):
        """Переопределение события по таймеру."""
        if self.setting.allow_widget:
            if self.track:
                self.__check_area()
            elif not self.activeDrag:
                self.count += 1
                if self.count > 30:
                    self.count = 0
                    self.track = True

    def mouseMoveEvent(self, e):
        """Переопределение события на дижение курсора."""
        self.__hide_image()
        self.hide()
        self.track = False
        e.accept()

    def dropEvent(self, e):
        """Переопределение события на отпускание перетаскиваемого файла в окно виджета."""
        e.accept()
        self.hide()
        self.__hide_image()
        self.track = True
        self.activeDrag = False

        data = e.mimeData()
        if data.hasFormat("text/uri-list"):
            self.file_manager.parsing_url(data.urls())
        elif data.hasFormat("text/plain"):
            self.file_manager.parsing_text(data.text())

    def dragLeaveEvent(self, e):
        """Переопределение события на покидание курсором зоны виджета при перетаскивании."""
        self.hide()
        self.__hide_image()
        self.track = True
        self.activeDrag = False
        e.accept()

    def dragEnterEvent(self, e):
        """Переопределение события на появление курсора в зоне виджета при перетаскивании."""
        self.__show_image()
        self.track = False
        self.activeDrag = True
        e.accept()

    def dragMoveEvent(self, e):
        """Переопределение события на перемещение курсора в области виджета при перетаскивании."""
        e.accept()

    def __mouseArea(self):
        """
        Метод для проверки нахождения курсора пользователя в области расположения виджета.
        :return: True - если курсор находитсья в зоне виджета
        """
        result = False
        pos = QCursor.pos()
        x_cursor = pos.x()
        y_cursor = pos.y()
        x_pos = self.x()
        y_pos = self.y()
        check_x = (x_pos + self.a) < x_cursor < (x_pos + self.wh - self.a)
        check_y = (y_pos + self.a) < y_cursor < (y_pos + self.wh - self.a)
        if check_x and check_y:
            result = True
        return result

    def __check_area(self):
        """Метод для переключения состояния виджета."""
        if self.__mouseArea():
            self.show()
        else:
            self.hide()

    def __show_image(self):
        """Метод для показа изображения виджета."""
        self.label.setPixmap(self.image)

    def __hide_image(self):
        """Метод для скрытия изображения виджета."""
        self.label.setPixmap(self.image2)

    def __set_position(self):
        """Метод для установки виджета в правый нижний угол экрана вне зависимости от разрешения экрана."""
        screen_geometry = QApplication.desktop().availableGeometry()
        screen_size = (screen_geometry.width(), screen_geometry.height())
        win_size = (self.frameSize().width(), self.frameSize().height())
        x = screen_size[0] - win_size[0] - 8
        y = screen_size[1] - win_size[1] - 8
        self.move(x, y)

    def __setupUI(self):
        """Построение окна."""
        self.setWindowFlags(Qt.CustomizeWindowHint |
                            Qt.FramelessWindowHint |
                            Qt.Tool |
                            Qt.WindowStaysOnTopHint)

        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowOpacity(0.6)
        self.setEnabled(True)

        self.setFixedSize(self.wh, self.wh)
        self.setWindowTitle(None)

        self.setMouseTracking(True)
        self.setAcceptDrops(True)

        self.image = QPixmap(":/app/icon.png")

        self.image = self.image.scaled(self.wh, self.wh, Qt.KeepAspectRatio,
                                       Qt.SmoothTransformation)

        self.image2 = QPixmap(":/app/empty.png")
        self.image2 = self.image2.scaled(self.wh, self.wh, Qt.KeepAspectRatio,
                                         Qt.SmoothTransformation)

        self.label = QLabel(self)
        self.label.setFixedSize(self.width(), self.height())
        self.label.setPixmap(self.image2)
        self.label.setMouseTracking(True)

        self.__set_position()
        self.timer = QBasicTimer()
