# -*- coding: utf-8 -*-
from PyQt5.QtCore import Qt, QSize, QRect, QEvent, pyqtSignal
from PyQt5.QtWidgets import QWidget, QMainWindow, QHBoxLayout, QLayout, QScrollArea, QLabel, QSpacerItem, QPushButton, QComboBox, \
    QLineEdit, QCompleter, QSizePolicy
from PyQt5.QtGui import QFontMetrics, QPixmap

# --------------------------QEdit--------------------------------------------

# def send_to_parents(target, evt):
#     app = QApplication.instance()
#     while target:
#         app.sendEvent(target, evt)
#         if not evt.isAccepted():
#             if hasattr(target, 'parent'):
#                 target = target.parent()
#         else:
#             target = None
#     return evt.isAccepted()
#
# class OpenFileInfoEvent(QEvent):
#     """docstring for OpenFileInfoEvent."""
#
#     def __init__(self):
#         self.id = id

# --------------------------QLabel--------------------------------------------

class QLabelClicked(QLabel):
    left_click = pyqtSignal()
    clicked = pyqtSignal()

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)

    def mousePressEvent(self, event):
        self.left_click.emit()
        self.clicked.emit()
        # print('left click')

class QLabelClickedStyleFirst(QLabelClicked):
    """docstring for QLabelClickedStyleFirst."""

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)
        self.setStyleSheet("")

    def setStyleSheet(self, style):
        QLabel.setStyleSheet(self, """QLabel {%s}
            QLabel:pressed {
                background-color: rgb(40, 46, 51);}
            QLabel:hover {
                background-color: rgb(61, 68, 75);}""" % (style))



class QLabelTagToSearch(QLabel):  # !!!!!!!!!!
    """docstring for QLabelTagToSearch."""
    clicked = pyqtSignal()

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)
        self.setStyleSheet("background-color: rgb(0, 150, 135);\n"
                           "color: rgb(229, 229, 229);\n"
                           "border-radius: 8px;")
        self.search_tag = None

    def addText(self, text):
        self.search_tag = text;
        text = f"#{text}"
        fm = QFontMetrics(self.font());
        pixelsWide = fm.horizontalAdvance(text);
        self.setMinimumSize(QSize(pixelsWide * 1.9 + 10, 25))
        self.setMaximumSize(QSize(pixelsWide * 1.9 + 10, 25))
        self.setAlignment(Qt.AlignCenter)
        self.setText(text)

    def mousePressEvent(self, event):
        self.clicked.emit()

    def setStyleSheet(self, style):
        QLabel.setStyleSheet(self, """QLabel {%s}
            QLabel:pressed {
                border-style: inset;
                background-color: rgba(0, 100, 255, 127);}
            QLabel:hover {
                background-color: rgba(0, 210, 255, 127);}""" % (style))


class QLabelEnd3Point(QLabel):
    """docstring for QLabelEnd3Point."""

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)

    def setText(self, text):
        fm = QFontMetrics(self.font())
        text = fm.elidedText(text, Qt.ElideRight, self.width() * 1.8)
        super().setText(text)

    # def resizeEvent(self, event):
    #     self.resized.emit()
    #     return super(QLabelEnd3Point, self).resizeEvent(event)


class QWidgetPictureScaled(QWidget):  # !!!!!!!!!!
    """docstring"""
    clicked = pyqtSignal()

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)
        self.layout = QHBoxLayout(self)
        self.layout.setSizeConstraint(QLayout.SetMaximumSize)
        self.Picture = QLabel(self)
        self.Picture.setAcceptDrops(False)
        self.Picture.setStyleSheet("background-image: url(:/background/background.jpg);")
        self.Picture.setScaledContents(True)
        self.Picture.setAlignment(Qt.AlignCenter)
        self.Picture.setMaximumSize(0, 0)
        self.layout.addWidget(self.Picture)

        self.pixmap = None
        self.path = None

    def setPixmap(self, pixmap):
        self.pixmap = pixmap
        self.Picture.setPixmap(pixmap)
        self.scaled()

    def setPicture(self, path):
        pixmap = QPixmap(path)
        self.pixmap = pixmap
        self.path = path
        self.Picture.setPixmap(pixmap)
        self.scaled()

    def clear(self):
        self.Picture.clear()

    def scaled(self):
        if self.pixmap:
            try:
                if self.pixmap.height() > self.pixmap.width():
                    width = self.width()
                    height = width * (self.pixmap.height() / self.pixmap.width())
                else:
                    height = self.height()
                    width = height * (self.pixmap.width() / self.pixmap.height())
            except Exception as e:
                width = 600
                height = 600

            if height > self.height():
                width = width * (self.height() / height)
                height = self.height()
            if width > self.width():
                height = height * (self.width() / width)
                width = self.width()
            self.Picture.setMaximumSize(width, height)

    def getPath(self):
        return self.path

    def resizeEvent(self, event):
        self.scaled()

    def mouseReleaseEvent(self, event):
        self.clicked.emit()


class QLabelHighLight(QLabel):
    """docstring for QLabelHighLight."""
    _highlighters = []
    clicked = pyqtSignal()

    def __init__(self, parent=None):
        QLabel.__init__(self, parent)
        QLabelHighLight._highlighters.append(self)
        self.setStyleSheet("")

    def mousePressEvent(self, event):
        self.clicked.emit()
        self.highlighters_change()
        self.setStyleSheet("border-style: none none solid none;")

    def highlighters_change(self):
        for highlighter in QLabelHighLight._highlighters:
            highlighter.setStyleSheet("border-style: none none none none;")

    def setStyleSheet(self, style):
        QLabel.setStyleSheet(self, """QLabel {%s}
            QLabel:hover {
                border-style: none none solid none;}""" % (style))

class QLabelUnderLine(QLabelClicked):
    """docstring for QLabelUnderLine."""

    def __init__(self, parent = None):
        QLabelClicked.__init__(self, parent)
        self.installEventFilter(self)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.Enter:
            font = self.font()
            font.setUnderline(True)
            self.setFont(font)
            return True
        if event.type() == QEvent.Leave:
            font = self.font()
            font.setUnderline(False)
            self.setFont(font)
            return True
        return super(QLabelUnderLine, self).eventFilter(obj, event)

    def hoverLeaveEvent(self, event):
        print(1)

# --------------------------QPushButton----------------------------------------

class QPushButtonMenu(QPushButton):
    _buttons = []

    def __init__(self, parent=None):
        QPushButton.__init__(self, parent)
        self.installEventFilter(self)
        QPushButtonMenu._buttons.append(self)
        self.setStyleSheet('QPushButton {background-color: rgb(61, 68, 75);}')
        self.change_after_hover = False

    def eventFilter(self, obj, event):
        if event.type() == QEvent.HoverEnter:
            if self.isFlat():
                self.setStyleSheet('QPushButton {background-color: rgb(61, 68, 75);}')
                self.setFlat(False)
                self.change_after_hover = True
            return True
        if event.type() == QEvent.HoverLeave:
            if self.change_after_hover:
                self.setStyleSheet('QPushButton {background-color: rgb(61, 68, 75);}')
                self.setFlat(True)
                self.change_after_hover = False
            return True
        return super(QPushButtonMenu, self).eventFilter(obj, event)

    def mousePressEvent(self, event):
        self.clicked.emit()
        self.buttons_change()
        self.setStyleSheet('QPushButton {background-color: rgb(61, 68, 75);}')
        self.setFlat(False)
        self.change_after_hover = False

    def buttons_change(self):
        for button in QPushButtonMenu._buttons:
            button.setFlat(True)

class QPushButtonStyleFirst(QPushButton):
    def __init__(self, parent=None):
        QPushButton.__init__(self, parent)
        self.setStyleSheet("")

    def setStyleSheet(self, style):
        QPushButton.setStyleSheet(self, """QPushButton {%s}
            QPushButton:pressed {
                border-style: inset;
                background-color: rgba(0, 100, 255, 127);}
            QPushButton:hover {
                background-color: rgba(0, 210, 255, 127);}""" % (style))

# --------------------------QCompleter-----------------------------------------

class QComboBoxCompleter(QComboBox):  # !!!!!!!!!!
    def __init__(self, parent):
        QComboBox.__init__(self, parent)
        # self.installEventFilter(self)

    # def eventFilter(self, obj, event):
    #     print(event)
    #     if event.type() == QEvent.MouseButtonPress:
    #         return True
    #     if event.type() == QEvent.MouseButtonRelease:
    #         return True
    #     return super().eventFilter(obj, event)


class QLineEditCompleter(QLineEdit):
    """docstring for QLineEditCompleter."""
    editingFinished = pyqtSignal()

    def __init__(self, arg):
        super(QLineEditCompleter, self).__init__()
        self.arg = arg

    def Compliter(self, wordList):
        completer = QCompleter(wordList, self)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        completer.setMaxVisibleItems(5)
        completer.setCompletionMode(QCompleter.PopupCompletion)
        completer.popup().setStyleSheet("background-color: rgb(61, 68, 75);\n"
                                        "color: rgb(229, 229, 229);\n" "font: 87 13pt \"Arial\";" "border-radius: 1px;\n")
        self.setCompleter(completer)

    def keyReleaseEvent(self, event):
        if (event.key() == Qt.Key_Return):
            self.editingFinished.emit()


# --------------------------QScrollArea--------------------------------------------

class QScrollAreaHorizontalTag(QScrollArea):
    """docstring for QScrollAreaHorizontalTag."""
    search = pyqtSignal()

    def __init__(self, parent=None):
        QScrollArea.__init__(self, parent)
        self.installEventFilter(parent)
        # self.Tag_Widget = parent
        self.Tag_Widget = QWidget()
        self.Tag_Widget.setGeometry(QRect(0, 0, 320, 35))
        self.Tag_Widget.setContextMenuPolicy(Qt.NoContextMenu)
        self.Tags_Layout = QHBoxLayout(self.Tag_Widget)
        self.Tags_Layout.setSizeConstraint(QLayout.SetMinimumSize)
        self.Tags_Layout.setContentsMargins(0, 2, 0, 15)
        self.setWidget(self.Tag_Widget)
        self.tags = []
        self.type = 0
        self.readOnly = False
        self.search_tag = None

    def eventFilter(self, obj, event):
        if event.type() == QEvent.Wheel:
            self.wheel_Event(event)
            return True
        return super(QScrollAreaHorizontalTag, self).eventFilter(obj, event)

    def wheel_Event(self, event):
        if (event.angleDelta().y() < 0):
            self.horizontalScrollBar().setValue(self.horizontalScrollBar().value() + 20)
            self.verticalScrollBar().setValue(0)
        if (event.angleDelta().y() > 0):
            self.horizontalScrollBar().setValue(self.horizontalScrollBar().value() - 20)
            self.verticalScrollBar().setValue(10)
        event.accept()

    def add_tags(self, tags: list, type=0):  # 0 - без фитч, 1 - самоудаление, 2 - самопоиск по тегу
        self.tags = []
        self.clearLayout(self.Tags_Layout)
        self.type = type
        self.tags = tags
        for i in range(len(tags)):
            # print(tags[i])
            if tags[i] == "all": continue
            label = QLabelTagToSearch(self.Tag_Widget)
            label.addText(tags[i])
            if type == 1:
                label.clicked.connect(self.delete_tag)
            if type == 2:
                label.clicked.connect(self._search)
            self.Tags_Layout.addWidget(label)

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.Tags_Layout.addItem(spacerItem)

    def delete_tag(self):
        if not self.readOnly:
            layout = self.Tags_Layout
            self.tags.remove(self.sender().text()[1:])
            self.add_tags(self.tags, self.type)

    def get_all_tags(self):
        # layout = self.Tags_Layout
        # tags = []
        # while layout.count():
        #     child = layout.takeAt(0)
        #     if child.widget():
        #         tag = child.widget().text()[1:]
        #         tags.append(tag)
        return self.tags

    def clearLayout(self, layout):
        while layout.count():
            child = layout.takeAt(0)
            if child.layout():
                self.clearLayout(child)
            if child.widget():
                child.widget().deleteLater()

    def clear(self):
        self.clearLayout(self.Tags_Layout)
        self.tags = []

    def _search(self):
        self.search_tag = self.sender().search_tag
        self.search.emit()

    def setReadOnly(self, state: bool):
        self.readOnly = state


class QScrollAreaSpace(QScrollArea):
    """docstring for QScrollAreaSpace."""

    def __init__(self, parent=None):
        QScrollArea.__init__(self, parent)

    def keyReleaseEvent(self, event):
        if (event.key() == Qt.Key_Space):
            self.verticalScrollBar().setValue(self.verticalScrollBar().value() + 200)
