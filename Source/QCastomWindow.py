from PyQt5 import QtCore, QtWidgets, QtGui

from Source.UI_RoundWindow import Ui_Window_Round

# from PyQt5 import Qt

class QRoundWindow(QtWidgets.QMainWindow):
    """
    Класс для отображение стилизированных рамок во всех окнах

    Аргументы:
        wdw - экземпляр класса, отвечающий за дизайн
        last_x - положение мышки относительно окна при перемещение по x
        last_y - положение мышки относительно окна при перемещение по y
        centralwidget - переопределение главного виджета, в который добавляются остальные виджеты

    Методы:
        setWindowTitle - отображение названия окна в рамке
        resize - переопределение стандартного метада resize с добавлением места под шапку окна
        mouseMoveEvent - перемещения окна
        mousePressEvent - фиксация положения мышки при нажатии
    """
    def __init__(self, parent = None):
        super().__init__()
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.wdw = Ui_Window_Round()
        self.wdw.setupUi(self)
        self.centralwidget = self.wdw.centralwidget
        self.last_x = None
        self.last_y = None
        self.is_Full_Screen = False
        self.is_Window_Move = False
        self.is_Resize = False

        self.wdw.Close.setMargin(3)
        self.wdw.Fullscreen.setMargin(3)
        self.wdw.Roll.setMargin(2)
        # self.installEventFilter(self.wdw.Fullscreen)
        self.installEventFilter(self)

        self.wdw.Close.clicked.connect(self.close)
        self.wdw.Fullscreen.clicked.connect(self.fullscreen_function)
        self.wdw.Roll.clicked.connect(self.showMinimized)

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            return False
        if event.type() == QtCore.QEvent.MouseButtonRelease:
    
            self.last_x = None
            self.last_y = None
            self.is_Window_Move = False
            self.is_Resize = False
            return False
        return super().eventFilter(obj, event)

    def fullscreen_function(self):
        if self.is_Full_Screen:
            self.showNormal()
            self.is_Window_Move = False
            self.is_Resize = False
            self.is_Full_Screen = False
        else:
            # self.showFullScreen()
            self.showMaximized()
            self.move(0,0)
            self.is_Resize = False
            self.is_Window_Move = False
            self.is_Full_Screen = True

    def setWindowTitle(self, title):
        self.wdw.Title.setText(title)

    # def resize(self, x, y):
    #    super().resize(x, y + )
    #    print(f"x:{x} y:{y}")

    def mouseMoveEvent(self, event):
        if self.last_x is None and self.last_y is None:
            self.is_Resize = False
            self.is_Window_Move = False
        if event and self.is_Resize:
            self.is_Window_Move = False
            delta_x = event.globalX() - self.x() - self.last_x
            delta_y = event.globalY() - self.y() - self.last_y
            # print(f"{delta_x} - {delta_y}")
            # print(f"x:{self.last_x} y:{self.last_y}")
            if self.last_x + 50 > self.width():
                self.resize(self.width() + delta_x, self.height())
            if self.last_y + 50 > self.height():
                self.resize(self.width(), self.height() + delta_y)
            if self.last_x < 50:
                if self.width() > self.minimumWidth():
                    self.move(event.globalX(), self.y())
                self.resize(self.width() - delta_x - self.last_x, self.height())
            if self.last_y < 50:
                if self.height() > self.minimumHeight():
                    self.move(self.x(), event.globalY())
                self.resize(self.width(), self.height() - delta_y - self.last_y)
            self.last_x = event.x()
            self.last_y = event.y()

        if event and self.is_Window_Move:
            x = event.globalX() - self.last_x
            y = event.globalY() - self.last_y
            self.move(x, y)

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.last_x = event.x()
            self.last_y = event.y()
            if event.y() < 30:
                self.is_Window_Move = True
            else:
                self.is_Window_Move = False

            if event.x() < 15 or event.x()+15 > self.width() or \
                    event.y() < 15 or event.y()+15 > self.height():
                self.is_Resize = True
            else:
                self.is_Resize = False

    def mouseReleaseEvent(self, event):
        self.last_x = None
        self.last_y = None
        self.is_Window_Move = False
        self.is_Resize = False
