# *- coding: utf-8 -*-
"""База данных программы."""

import sqlite3
from collections import namedtuple


class DataBaseManager:
    """Класс для работы с базой данных SQLite 3"""

    _data_base_name = "Tabs.db"
    no_tags = "Nope"
    no_group = "Nope"
    no_format = "Nope"

    def __new__(cls):
        """Класс является синглетоном."""
        if not hasattr(cls, 'instance'):
            cls.instance = super(DataBaseManager, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.conn = sqlite3.connect(self._data_base_name)
        self.cursor = self.conn.cursor()

        Files = namedtuple("Files", ["tName", "Id", "Title", "DataTime", "FPath", "GroupId",
                                     "TIPath", "FormatId", "ExtensionId", "Description", "CopyNumber"])
        self._tFiles = Files("Files", "Id", "Title", "DataTime", "FPath", "GroupId",
                             "TIPath", "FormatId", "ExtensionId", "Description", "CopyNumber")

        Tags = namedtuple("Tags", ["tName", "Id", "FileId", "Tag"])
        self._tTags = Tags("Tags", "Id", "FileId", "Tag")

        Formats = namedtuple("Formats", ["tName", "Id", "Format"])
        self._tFormats = Formats("Formats", "Id", "Format")

        Groups = namedtuple("Groups", ["tName", "Id", "Group"])
        self._tGroups = Groups("Groups", "Id", "FGroup")

        Extensions = namedtuple("Extensions", ["tName", "Id", "Extension"])
        self._tExtensions = Extensions("Extensions", "Id", "Extension")

        self.__create_Extensions_table()
        self.__create_Formats_table()
        self.__create_Groups_table()

        self.__create_Files_table()

        self.__create_Tags_table()

    def __create_Files_table(self):
        params = self._tFiles
        query = f"""CREATE TABLE IF NOT EXISTS {params.tName} (
                    {params.Id} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    {params.Title} TEXT,
                    {params.DataTime} TEXT NOT NULL,
                    {params.FPath} TEXT NOT NULL,
                    {params.TIPath} TEXT,
                    {params.FormatId} INTEGER,
                    {params.ExtensionId} INTEGER NOT NULL,
                    {params.GroupId} INTEGER,
                    {params.Description} TEXT,
                    {params.CopyNumber} INTEGER,
                    FOREIGN KEY ({params.ExtensionId}) REFERENCES {self._tExtensions.tName}({self._tExtensions.Id})
                    FOREIGN KEY ({params.FormatId}) REFERENCES {self._tFormats.tName}({self._tFormats.Id})
                    FOREIGN KEY ({params.GroupId}) REFERENCES {self._tGroups.tName}({self._tGroups.Id})
                    );"""
        self.cursor.execute(query)
        self.conn.commit()

    def __create_Tags_table(self):
        query = f"""CREATE TABLE IF NOT EXISTS {self._tTags.tName} (
                    {self._tTags.Id} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    {self._tTags.FileId} INTEGER NOT NULL,
                    {self._tTags.Tag} TEXT NOT NULL,
                    FOREIGN KEY ({self._tTags.FileId}) REFERENCES {self._tFiles.tName}({self._tFiles.Id})
                    );"""
        self.cursor.execute(query)
        self.conn.commit()

    def __create_Formats_table(self):
        query = f"""CREATE TABLE IF NOT EXISTS {self._tFormats.tName} (
                    {self._tFormats.Id} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    {self._tFormats.Format} TEXT NOT NULL
                    );"""
        self.cursor.execute(query)
        self.conn.commit()

    def __create_Groups_table(self):
        query = f"""CREATE TABLE IF NOT EXISTS {self._tGroups.tName} (
                    {self._tGroups.Id} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    {self._tGroups.Group} TEXT NOT NULL
                    );"""
        self.cursor.execute(query)
        self.conn.commit()

    def __create_Extensions_table(self):
        query = f"""CREATE TABLE IF NOT EXISTS {self._tExtensions.tName} (
                    {self._tExtensions.Id} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    {self._tExtensions.Extension} TEXT NOT NULL
                    );"""
        self.cursor.execute(query)
        self.conn.commit()

    def __delete_tables(self):
        query = f"DROP TABLE IF EXISTS {self._tGroups.tName};"
        self.cursor.execute(query)
        query = f"DROP TABLE IF EXISTS {self._tFormats.tName};"
        self.cursor.execute(query)
        query = f"DROP TABLE IF EXISTS {self._tFiles.tName};"
        self.cursor.execute(query)
        query = f"DROP TABLE IF EXISTS {self._tTags.tName};"
        self.cursor.execute(query)
        query = f"DROP TABLE IF EXISTS {self._tExtensions.tName};"
        self.cursor.execute(query)
        self.conn.commit()

    def close(self):
        self.cursor.close()
        self.conn.close()

    #
    # E - Extensions
    # F - Formats
    # G - Groups
    #

    # возвращает Id значения
    def __get_EFG_Id(self, tName, fieldId, fieldMain, value):
        query = f"""SELECT {fieldId} FROM {tName} WHERE {fieldMain} == '{value}';"""
        self.cursor.execute(query)
        list_ID = self.cursor.fetchall()

        if not list_ID:
            query = f"""INSERT INTO {tName} ({fieldMain}) VALUES ('{value}');"""
            self.cursor.execute(query)
            query = f"""SELECT {fieldId} FROM {tName} WHERE {fieldMain} == '{value}';"""
            self.cursor.execute(query)
            list_ID = self.cursor.fetchall()

        self.conn.commit()
        return list_ID[0][0]

    def __get_ExtensionId(self, file_extension):
        return self.__get_EFG_Id(self._tExtensions.tName, self._tExtensions.Id,
                                 self._tExtensions.Extension, file_extension)

    def __get_FormatId(self, file_format):
        return self.__get_EFG_Id(self._tFormats.tName, self._tFormats.Id,
                                 self._tFormats.Format, file_format)

    def __get_GroupId(self, file_group):
        return self.__get_EFG_Id(self._tGroups.tName, self._tGroups.Id,
                                 self._tGroups.Group, file_group)

    #

    # возвращает значение по Id
    def __get_EFG_by_Id(self, tName, fieldId, fieldMain, value):
        query = f"""SELECT {fieldMain} FROM {tName} WHERE {fieldId} == '{value}';"""
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][0]

    def __get__Extension_by_Id(self, extension_id):
        return self.__get_EFG_by_Id(self._tExtensions.tName, self._tExtensions.Id,
                                    self._tExtensions.Extension, extension_id)

    def __get_Format_by_Id(self, format_id):
        return self.__get_EFG_by_Id(self._tFormats.tName, self._tFormats.Id,
                                    self._tFormats.Format, format_id)

    def __get_Group_by_Id(self, group_id):
        return self.__get_EFG_by_Id(self._tGroups.tName, self._tGroups.Id,
                                    self._tGroups.Group, group_id)

    #

    # возвращает все уникальные записи
    def __get_all_EFG(self, tName, fieldMain):
        query = f"""SELECT DISTINCT {fieldMain} FROM {tName} ORDER BY {fieldMain} ASC;"""
        self.cursor.execute(query)
        list_EFG = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_EFG

    def get_all_Extensions(self):
        return self.__get_all_EFG(self._tExtensions.tName, self._tExtensions.Extension)

    def get_all_Formats(self):
        return self.__get_all_EFG(self._tFormats.tName, self._tFormats.Format)

    def get_all_Groups(self):
        return self.__get_all_EFG(self._tGroups.tName, self._tGroups.Group)

    #

    # добавляет значения из списка, если их нет в бд
    def __fill_EFG(self, tName, fieldMain, new_list_EFG: list):
        list_EFG = self.__get_all_EFG(tName, fieldMain)
        for value in new_list_EFG:
            if value not in list_EFG:
                query = f"""INSERT INTO {tName} ({fieldMain}) VALUES ('{value}');"""
                self.cursor.execute(query)

        self.conn.commit()

    def fill_Extensions(self, extensions: list):
        self.__fill_EFG(tName=self._tExtensions.tName, fieldMain=self._tExtensions.Extension, new_list_EFG=extensions)

    def fill_Formats(self, formats: list):
        self.__fill_EFG(tName=self._tFormats.tName, fieldMain=self._tFormats.Format, new_list_EFG=formats)

    def fill_Groups(self, groups: list):
        self.__fill_EFG(tName=self._tGroups.tName, fieldMain=self._tGroups.Group, new_list_EFG=groups)

    #
    # Tags
    #

    # Получение всех тегов по Id
    def get_all_tags_by_Id(self, file_id):
        query = f"""SELECT DISTINCT {self._tTags.Tag} FROM {self._tTags.tName}
                    WHERE {self._tTags.FileId} == '{file_id}' ORDER BY {self._tTags.Tag} ASC;"""
        self.cursor.execute(query)
        list_tags = [t[0] for t in self.cursor.fetchall()]
        self.conn.commit()
        return list_tags

    # Получение всех используемых тегов
    def get_all_tags(self):
        query = f"""SELECT DISTINCT {self._tTags.Tag} FROM {self._tTags.tName} ORDER BY {self._tTags.Tag} ASC;"""
        self.cursor.execute(query)
        list_tags = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_tags

    # Добавить тег или набор тегов по Id
    def add_tags_by_Id(self, file_id: int, tags):
        if not isinstance(tags, list):
            tags = [tags]

        list_tags = self.get_all_tags_by_Id(file_id)
        for tag in tags:
            if tag not in list_tags:
                query = f"""INSERT INTO {self._tTags.tName} ('{self._tTags.FileId}', '{self._tTags.Tag}')
                            VALUES ('{file_id}', '{tag}');"""
                self.cursor.execute(query)

        if self.no_tags in list_tags:
            self.del_tag_by_Id(file_id=file_id, tag=self.no_tags)
        self.conn.commit()

    # Удаление тега по Id
    def del_tag_by_Id(self, file_id, tag):
        query = f"""DELETE FROM {self._tTags.tName}
                    WHERE {self._tTags.FileId} == '{file_id}' AND {self._tTags.Tag} == '{tag}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Удаление всех тегов по Id
    def del_all_tags_by_Id(self, file_id):
        query = f"""DELETE FROM {self._tTags.tName} WHERE {self._tTags.FileId} == '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()
        self.add_tags_by_Id(file_id=file_id, tags=self.no_tags)

    # Возвращяет Id всех файлов без тега
    def get_all_Id_with_no_tag(self):
        return self.get_Id_by_tags([self.no_tags])

    def update_tags_by_Id(self, file_id, new_tags = None):
        if not new_tags:
            new_tags = self.no_tags
        if not isinstance(new_tags, list):
            new_tags = [new_tags]
        if not new_tags:
            self.del_all_tags_by_Id(file_id)
            return None
        old_tags = self.get_all_tags_by_Id(file_id)

        for tag in new_tags:
            if tag not in old_tags:
                self.add_tags_by_Id(file_id=file_id, tags=tag)
        for tag in old_tags:
            if tag not in new_tags:
                self.del_tag_by_Id(file_id=file_id, tag=tag)

    #
    # Files
    #

    # Добавить информацию о файле
    def add_file(self, title: str, data_time: str, path: str, extension: str, ti_path: str = None,
                 group: str = no_group, f_format: str = no_format, description: str = "None",  tags: list = None):
        if not tags:
            tags = self.no_tags 
        formatId = self.__get_FormatId(f_format)
        extensionId = self.__get_ExtensionId(extension)
        groupId = self.__get_GroupId(group)

        query = f"""INSERT INTO {self._tFiles.tName} (
                    {self._tFiles.Title}, {self._tFiles.DataTime}, {self._tFiles.FPath}, {self._tFiles.GroupId},
                    {self._tFiles.TIPath}, {self._tFiles.FormatId}, {self._tFiles.ExtensionId},
                    {self._tFiles.Description}, {self._tFiles.CopyNumber}
                    ) VALUES (
                    '{title}', '{data_time}', '{path}', '{groupId}',
                    '{ti_path}', '{formatId}', '{extensionId}',
                    '{description.upper()}', '{0}'
                    );"""
        self.cursor.execute(query)
        query = f"""SELECT {self._tFiles.Id} FROM {self._tFiles.tName}
                    WHERE {self._tFiles.DataTime} == '{data_time}';"""
        self.cursor.execute(query)
        file_id = self.cursor.fetchall()[-1][0]
        self.conn.commit()
        self.add_tags_by_Id(file_id=file_id, tags=tags)

    # Удаление файла из бд по Id
    def del_file_by_Id(self, file_id):
        query = f"""DELETE FROM {self._tFiles.tName} WHERE {self._tFiles.Id} == '{file_id}';"""
        self.cursor.execute(query)
        query = f"""DELETE FROM {self._tTags.tName} WHERE {self._tTags.FileId} == '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Возвращаяет Id всех файлов
    def get_all_Id(self, limit: int = -1):
        if limit == -1:
            query = f"""SELECT DISTINCT {self._tFiles.Id} FROM {self._tFiles.tName}
                        ORDER BY {self._tFiles.DataTime} DESC;"""
        else:
            query = f"""SELECT DISTINCT {self._tFiles.Id} FROM {self._tFiles.tName}
                        ORDER BY {self._tFiles.DataTime} DESC LIMIT {limit};"""
        self.cursor.execute(query)
        list_fileID = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        "ПОЛУЧЕНИЕ ВСЕХ АЙДИ"
        return list_fileID

    # Получить информацию о файле по Id
    def get_information_by_Id(self, file_id):
        Fields = namedtuple("Info", ["Id", "Title", "DataTime", "FPath", "TIPath", "Format",
                                     "Extension", "Group", "Description", "CopyNumber", "Tags"])
        format_pos = 5
        extension_pos = 6
        group_pos = 7

        query = f"""SELECT * FROM {self._tFiles.tName} WHERE {self._tFiles.Id} == '{file_id}';"""
        self.cursor.execute(query)
        information = [i for i in self.cursor.fetchall()[0]]
        self.conn.commit()

        format_id = information[format_pos]
        extension_id = information[extension_pos]
        group_id = information[group_pos]

        information[format_pos] = self.__get_Format_by_Id(format_id)
        information[extension_pos] = self.__get__Extension_by_Id(extension_id)
        information[group_pos] = self.__get_Group_by_Id(group_id)

        information.append(self.get_all_tags_by_Id(file_id))
        info = Fields._make(information)
        return info

    # Узменяет информацию о файле по кортежу
    def update_information(self, info):
        format_id = self.__get_FormatId(info.Format)
        extension_id = self.__get_ExtensionId(info.Extension)
        group_id = self.__get_GroupId(info.Group)

        self.update_tags_by_Id(info.Id, info.Tags)

        query = f"""UPDATE {self._tFiles.tName} SET
                    {self._tFiles.Title} = '{info.Title}',
                    {self._tFiles.DataTime} = '{info.DataTime}',
                    {self._tFiles.FPath} = '{info.FPath}',
                    {self._tFiles.TIPath} = '{info.TIPath}',
                    {self._tFiles.FormatId} = '{format_id}',
                    {self._tFiles.ExtensionId} = '{extension_id}',
                    {self._tFiles.GroupId} = '{group_id}',
                    {self._tFiles.Description} = '{info.Description.upper()}'
                    WHERE {self._tFiles.Id} = '{info.Id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Увеличить количество копий на 1
    def increase_CopyNumber_by_Id(self, file_id):
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.CopyNumber}={self._tFiles.CopyNumber}+1
                    WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    #
    #   SEARCH
    #

    # Получение Id по тегам
    def get_Id_by_tags(self, tags: list):
        if not isinstance(tags, list):
            tags = [tags]

        all_tags = "'" + "', '".join(tags) + "'"
        query = f"""SELECT {self._tTags.FileId} FROM {self._tTags.tName}
                    WHERE {self._tTags.Tag} IN ({all_tags}) GROUP BY {self._tTags.FileId}
                    HAVING COUNT({self._tTags.FileId}) == {len(tags)} ORDER BY {self._tTags.FileId} DESC;"""
        self.cursor.execute(query)
        list_Id = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_Id

    # Получение Id по группе
    def get_Id_by_group(self, group: str = no_group):
        groupId = self.__get_GroupId(group)
        query = f"""SELECT {self._tFiles.Id} FROM {self._tFiles.tName}
                    WHERE {self._tFiles.GroupId} == '{groupId}' ORDER BY {self._tFiles.DataTime} DESC;"""
        self.cursor.execute(query)
        list_Id = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_Id

    # Получение Id по расширению
    def get_Id_by_extension(self, extension: str):
        extensionId = self.__get_ExtensionId(extension)
        query = f"""SELECT {self._tFiles.Id} FROM {self._tFiles.tName}
                    WHERE {self._tFiles.ExtensionId} == '{extensionId}' ORDER BY {self._tFiles.DataTime} DESC;"""
        self.cursor.execute(query)
        list_Id = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_Id

    # Получение Id по формату
    def get_Id_by_format(self, f_format: str = no_format):
        formatId = self.__get_FormatId(f_format)
        query = f"""SELECT {self._tFiles.Id} FROM {self._tFiles.tName}
                    WHERE {self._tFiles.FormatId} == '{formatId}' ORDER BY {self._tFiles.DataTime} DESC;"""
        self.cursor.execute(query)
        list_Id = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_Id

    # Поиск Id по описанию
    def get_Id_by_description(self, substring: str):
        query = f"""SELECT {self._tFiles.Id} FROM {self._tFiles.tName}
                    WHERE {self._tFiles.Description} LIKE '%{substring.upper()}%'
                    ORDER BY {self._tFiles.DataTime} DESC;"""
        self.cursor.execute(query)
        list_Id = [i[0] for i in self.cursor.fetchall()]
        self.conn.commit()
        return list_Id

    # Поиск Id по набору параметров
    def search(self, tags: list = None, description: str = None, group: str = None,
               f_format: str = None, extension: str = None):
        if tags is None and description is None and group is None and f_format is None and extension is None:
            return []
        else:
            result = self.get_all_Id()
            if tags is not None:
                result = list(set(result) & set(self.get_Id_by_tags(tags)))
            if description is not None:
                result = list(set(result) & set(self.get_Id_by_description(description)))
            if group is not None:
                result = list(set(result) & set(self.get_Id_by_group(group)))
            if f_format is not None:
                result = list(set(result) & set(self.get_Id_by_format(f_format)))
            if extension is not None:
                result = list(set(result) & set(self.get_Id_by_extension(extension)))
            return result

    #
    # Updates
    #

    # Обновить заголовок по Id
    def update_Title_by_Id(self, file_id: int, value: str):
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.Title} = '{value}'
                    WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить дату добавления по Id
    def update_DataTime_by_Id(self, file_id: int, value: str):
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.DataTime} = '{value}'
                    WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить путь до файла по Id
    def update_FPath_by_Id(self, file_id: int, value: str):
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.FPath} = '{value}'
                    WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить путь до миниатюры изображения по Id
    def update_TIPath_by_Id(self, file_id: int, value: str):
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.TIPath} = '{value}'
                    WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить формат файла по Id
    def update_Format_by_Id(self, file_id: int, value: str):
        format_id = self.__get_FormatId(value)
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.FormatId} = '{format_id}'
                       WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить группу файла по Id
    def update_Group_by_Id(self, file_id: int, value: str):
        group_id = self.__get_GroupId(value)
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.GroupId} = '{group_id}'
                       WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить расширение файла по Id
    def update_Extension_by_Id(self, file_id: int, value: str):
        extension_id = self.__get_ExtensionId(value)
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.ExtensionId} = '{extension_id}'
                           WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()

    # Обновить описание файла по Id
    def update_Description_by_Id(self, file_id: int, value: str):
        query = f"""UPDATE {self._tFiles.tName} SET {self._tFiles.Description} = '{value.upper()}'
                WHERE {self._tFiles.Id} = '{file_id}';"""
        self.cursor.execute(query)
        self.conn.commit()
