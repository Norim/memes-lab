# *- coding: utf-8 -*-
"""Функции для работы с расширениями файлов"""

# IMAGE FILES
img_1 = ["png", "bmp", "jpeg", "jpg", "gif", "ico", "pict", "psd", "tiff", "esp", "pcx", "svg"]
img_2 = ["cdr", "ai", "raw", "cr2", "nef", "dng", "webp", "heik"]
img_3 = ["jp2", "j2k", "jpf", "jpm", "jpg2", "j2c", "jpc", "jxr"]
img_4 = ["hdp", "wdp", "eps", "emz", "mix", "odg", "pd", "vsd", "wmf", "wpg"]

# AUDIO FILES
audio_1 = ["pcm", "wav", "aiff", "flac", "alac", "ape", "mp3", "aac", "ogg", "wma"]
audio_2 = ["mpga", "3ga", "aa", "aax", "aif", "aifc", "amr", "ape", "asx", "au", "aup", "awb", "caf", "cda", "gsm"]
audio_3 = ["iff", "kar", "koz", "m3u8", "m4a", "m4b", "m4p", "m4r", "mid", "midi", "mp2", "mpa", "mpc", "nfa", "oga"]
audio_4 = ["oma", "opus", "qcp", "ra", "ram", "rta", "xspf"]

# VIDEO FILES
video_1 = ["mp4", "mkv", "mov", "flv", "vob", "3gp", "mpeg", "mpg", "wmv", "avi", "amv", "m4v", "m2ts", "mts"]
video_2 = ["264", "h264", "amv", "3gpp", "ogv", "webm", "wlmp", "swf", "rm", "rmvb"]
video_3 = ["3g2", "aep", "arf", "asf", "bik", "ced", "cpi", "dav", "dir"]
video_4 = ["divx", "dvsd", "esp3", "f4v", "ifo", "imoviemobile", "mepx", "mod", "mswmm", "mxf", "nfv", "pds", "gt"]
video_5 = ["rcproject", "srt", "thp", "ts", "tvs", "veg", "vep", "vob", "vpj", "vpoj", "xesc"]

# ARCHIVE FILES
arch_1 = ["7z", "rar", "zip", "ace", "r01", "jar", "zipx"]
arch_2 = ["apk", "deb", "gz", "tar", "tar.gz", "rpm", "tgz"]
arch_3 = ["bz2", "crx", "dd", "gzip", "mht", "sit", "sitx", "snb"]

# BOOKS
book_1 = ["fb2", "epub", "mobi", "ibook", "lit", "djvu"]
book_2 = ["acsm", "apnx", "azw", "azw3", "cbr", "cbt", "cbz", "lit", "mbp", "opf", "prc", "tcr", "vbk"]

# DOCUMENTS
doc_0 = ["txt", "pdf", "xml", "rtf", "odt"]
doc_MSWord = ["doc", "docm", "docx", "dot", "dotm", "dotx", "htm", "html", "mht", "mhtml"]
doc_1 = ["log", "md", "pmd", "sxw", "tex"]
doc_2 = ["chm", "eml", "msg", "oxps", "pub", "shs", "wps", "xps"]
doc_3 = ["fdxt", "hwp", "odm"]
doc_4 = ["csk", "m3u", "pages", "pages.zip"]
doc_5 = ["vmg", "vnt", "wp5", "wpd"]

tab_1 = ["csv", "ods", "xlr", "xls", "xlsb", "xlsm", "xlsx"]
tab_MSExcel = ["xltx", "xltm", "xlt", "prn", "dif", "slk", "xlam", "xla"]
tab_2 = ["numbers", "wk3", "wks"]

pres_1 = ["pps", "ppsx", "ppt", "pptm", "pptx"]
pres_MSPower = ["potx", "potm", "pot", "thmx", "ppsm", "ppam", "ppa"]
pres_2 = ["flipchart", "key", "key.zip", "odp"]

anydoc = doc_0 + doc_MSWord + doc_1 + doc_2 + doc_3 + doc_4 + doc_5
anydoc += tab_1 + tab_MSExcel + tab_2 + pres_1 + pres_2 + pres_MSPower


def getExt(path: str):
    """
    Получение расширения файла (без точки, нижний регистр)
    :param path: путь к файлу
    :return: str() - расширение файла
    """
    deny_ext = ["", " ", " "]
    ext = path.split(".")
    ext = ext[-1]
    ext = ext.replace(" ", "")
    ext = ext.replace(" ", "")
    ext = ext.replace("\n", "")
    ext = ext.replace("\t", "")
    ext = ext.lower()
    if ext == path or ext in deny_ext:
        ext = ""
    return ext


def isNoneType(path: str):
    """
    Проверка файла на отсутствие расширения
    :param path: путь к файлу
    :return: True если файл не имеет расшиерния, иначе False
    """
    ext = getExt(path)
    if ext == "":
        return True
    else:
        return False


def isArchive(path: str):
    """
    Проверка, имеет ли файл расширение архива
    :param path: путь к файлу
    :return: True - если файл является архивом
    """
    ext = getExt(path)
    if ext in arch_1:
        return True
    elif ext in arch_2:
        return True
    elif ext in arch_3:
        return True
    else:
        return False


def isBook(path: str):
    """
    Проверка, имеет ли файл расширение книг
    :param path: путь к файлу
    :return: True - если файл является книгой
    """
    ext = getExt(path)
    if ext in book_1:
        return True
    elif ext in book_2:
        return True
    else:
        return False


def isAudio(path: str):
    """
    Проверка, имеет ли файл расширение аудио файлов
    :param path: путь к файлу
    :return: True - если аудио файл
    """
    ext = getExt(path)
    if ext in audio_1:
        return True
    elif ext in audio_2:
        return True
    elif ext in audio_3:
        return True
    elif ext in audio_4:
        return True
    else:
        return False


def isVideo(path: str):
    """
    Проверка, имеет ли файл расширение видео
    :param path: путь к файлу
    :return: True - если файл является видео
    """
    ext = getExt(path)
    if ext in video_1:
        return True
    elif ext in video_2:
        return True
    elif ext in video_3:
        return True
    elif ext in video_4:
        return True
    elif ext in video_5:
        return True
    else:
        return False


def isImage(path: str):
    """
    Проверка, имеет ли файл расширение изображения
    :param path: путь к файлу
    :return: True - если файл является изображением
    """
    ext = getExt(path)
    if ext in img_1:
        return True
    elif ext in img_2:
        return True
    elif ext in img_3:
        return True
    elif ext in img_4:
        return True
    else:
        return False


def isDocument(path: str):
    """
    Проверка, имеет ли файл расширение документа
    :param path: путь к файлу
    :return: True - если файл является документом
    """

    ext = getExt(path)
    if ext in anydoc:
        return True
    else:
        return False


def isMSOffice(path: str):
    """
    Проверка, имеет ли файл расширение документа MS Office
    :param path: путь к файлу
    :return: True - если файл является документом MS Office
    """

    if isMSExcel(path):
        return True
    elif isMSWord(path):
        return True
    else:
        return False


def isMSWord(path: str):
    """
    Проверка, имеет ли файл расширение документа MS Word
    :param path: путь к файлу
    :return: True - если файл является документом MS Word
    """
    ext = getExt(path)
    if ext in doc_0:
        return True
    elif ext in doc_MSWord:
        return True
    else:
        return False


def isMSExcel(path: str):
    """
    Проверка, имеет ли файл расширение документа MS Excel
    :param path: путь к файлу
    :return: True - если файл является документом MS Excel
    """
    ext = getExt(path)
    if ext in tab_1:
        return True
    elif ext in tab_MSExcel:
        return True
    else:
        return False


def isMSPower(path: str):
    """
    Проверка, имеет ли файл расширение документа MS PowerPoint
    :param path: путь к файлу
    :return: True - если файл является документом MS PowerPoint
    """
    ext = getExt(path)
    if ext in pres_1:
        return True
    elif ext in pres_MSPower:
        return True
    else:
        return False
