# *- coding: utf-8 -*-
"""Редактор изображений."""

import time

from PyQt5.QtCore import Qt, pyqtSignal, QRect, QPoint
from PyQt5.QtGui import QCursor, QColor, QPainter, QPen, QPixmap, QIcon, QFont
from PyQt5.QtWidgets import QSpacerItem, QSizePolicy, QWidget, QAction, QMenu, QVBoxLayout, QHBoxLayout, QLabel, \
    QPushButton, QSpinBox, QTextEdit, QScrollArea, QSlider

from Source.fun_ThumbnailCreator import create_thumbnail_image


class MemPaint(QWidget):
    """Класс редактора изображений."""
    closed = pyqtSignal()

    def __new__(cls, parent=None):
        """Класс является синглетоном."""
        if not hasattr(cls, 'instance'):
            cls.instance = super(MemPaint, cls).__new__(cls)
        return cls.instance

    def __init__(self, parent=None):
        """Инициализация окна редактора."""
        super().__init__()
        self.parent = parent

        self.setWindowTitle("PyPaint")
        self.resize(800, 600)

        self.colors = _ColorPalette(self)
        self.tools = _ToolBox(self)
        self.tools_menu = _ToolMenu(self)
        self.draw_area = _DrawLabel(self)

        self.image_source_path = None
        self.image_ext = None
        self.last_save = None

        self.__setupUI()

    @property
    def db(self):
        return self.parent.database

    def save(self):
        """Метод для сохранения отредактированного изображения."""
        if self.image_ext.lower() in ["gif", "bmp"]:
            self.image_ext = "PNG"
        self.draw_area.prepare_to_save(self.image_ext)
        t = time.strftime("%Y%m%d_%H%M%S", time.localtime())
        path = f"Media\\Image\\IMG_{t}.{self.image_ext.lower()}"
        self.last_save = path
        image = self.draw_area.pixmap_last.toImage()
        image.save(path, self.image_ext, quality=100)

        spath = create_thumbnail_image(path, f"IMG_{t}.{self.image_ext.lower()}")

        list = self.db.get_Id_by_tags("#new#")
        if not list is None:  # удаляю мусорные id_шники, если такие есть
            for id in list:
                tags = self.db.get_all_tags_by_Id(id)
                tags.remove("#new#")
                self.db.update_tags_by_Id(id, tags)
        self.db.add_file("Untitle", t, path, self.image_ext.lower(), spath, tags="#new#")

        self.closed.emit()
        self.__reset()
        self.close()

    def set_image(self, path_image):
        """
        Мето для установки редактируемого изображения
        :param path_image: путь к изображению
        :return: None
        """
        self.image_source_path = path_image
        self.last_save = None
        self.set_image_ext()
        self.draw_area.set_pixmap(path_image)

    def set_image_ext(self, ext=None):
        """
        Метод для установки расширения файла изображения.
        :param ext: расширение файла (без точки)
        :return: None
        """
        if ext != None:
            self.image_ext = ext.upper()
        else:
            self.image_ext = self.image_source_path.split(".")[-1].upper()

    def mousePressEvent(self, e):
        """Переопределение события при нажании на ЛКМ и ПКМ."""
        e.accept()

        if e.button() == Qt.RightButton:
            self.tools_menu.move(QCursor.pos().x(), QCursor.pos().y())
            self.tools_menu.show()

    def closeEvent(self, e):
        self.tools.text_editor.close()
        e.accept()

    def __reset(self):
        self.tools_menu.reset()
        self.tools.reset()
        self.colors.reset()
        self.draw_area.reset()

    def __setupUI(self):
        """Метод для построения интерфейса редактора."""
        self.HLayout = QHBoxLayout()
        self.VLayout = QVBoxLayout()
        self.VLayout.addLayout(self.HLayout)
        self.setLayout(self.VLayout)

        self.scroll_area = QScrollArea()
        self.scroll_area.setWidget(self.draw_area)

        self.HLayout.addWidget(self.scroll_area)
        self.HLayout.addLayout(self.colors)

        self.button_undo = QPushButton("Отменить")
        self.button_undo.setObjectName("ButtonCancel")
        self.button_undo.setFixedSize(170, 32)
        self.button_undo.clicked.connect(self.draw_area.image_undo)

        self.button_save = QPushButton("Сохранить")
        self.button_save.setObjectName("ButtonOK")
        self.button_save.setFixedSize(170, 32)
        self.button_save.clicked.connect(self.save)

        self.colors.addLayout(self.tools)
        self.colors.addWidget(self.button_undo)
        self.colors.addWidget(self.button_save)


class _ToolMenu(QMenu):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.current_tool = "Pen"
        self.__setupUI()

    def reset(self):
        self.current_tool = "Pen"

    def __set_tool_pen(self):
        """Метод для выбора инструмента Карандаш."""
        self.current_tool = "Pen"
        self.close()

    def __set_tool_line(self):
        """Метод для выбора инструмента Линия."""
        self.current_tool = "Line"
        self.close()

    def __set_tool_text(self):
        """Метод для выбора инструмента Текст."""
        self.current_tool = "Text"
        self.close()

    def __set_tool_eraser(self):
        self.current_tool = "Eraser"
        self.close()

    def __setupUI(self):
        self.action_set_pen = QAction(self)
        self.action_set_pen.setText("Карандаш")
        self.action_set_pen.setIcon(QIcon(":/PyPaint/Pen.png"))
        self.action_set_pen.triggered.connect(self.__set_tool_pen)
        self.addAction(self.action_set_pen)

        self.action_set_eraser = QAction(self)
        self.action_set_eraser.setText("Ластик")
        self.action_set_eraser.setIcon(QIcon(":/PyPaint/Eraser.png"))
        self.action_set_eraser.triggered.connect(self.__set_tool_eraser)
        self.addAction(self.action_set_eraser)

        self.action_set_line = QAction(self)
        self.action_set_line.setText("Линия")
        self.action_set_line.setIcon(QIcon(":/PyPaint/Line.png"))
        self.action_set_line.triggered.connect(self.__set_tool_line)
        self.addAction(self.action_set_line)

        self.action_set_text = QAction(self)
        self.action_set_text.setText("Текст")
        self.action_set_text.setIcon(QIcon(":/PyPaint/Text.png"))
        self.action_set_text.triggered.connect(self.__set_tool_text)
        self.addAction(self.action_set_text)


class _ToolBox(QVBoxLayout):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.text_editor = _TextEditor(parent)

        self.point_size = 14
        self.point_size_min = 1
        self.point_size_max = 200
        self.point_form = Qt.RoundCap

        self.__setupUI()

    @property
    def text(self):
        return self.text_editor.text

    def reset(self):
        self.point_size = 14
        self.spin_point_size.setValue(self.point_size)
        self.text_editor.reset()

    def __set_point_size(self):
        """
        Метод для установки размера рисуемых точек.
        :param num: int() - размер точки
        :return: None
        """
        self.point_size = self.spin_point_size.value()

    def __set_point_form(self):
        if self.point_form == Qt.RoundCap:
            self.point_form = Qt.SquareCap
        elif self.point_form == Qt.SquareCap:
            self.point_form = Qt.RoundCap
        else:
            self.point_form = Qt.RoundCap

    def __setupUI(self):
        self.spin_label = QLabel("Размер точек:")
        self.spin_label.setAlignment(Qt.AlignCenter)
        self.addWidget(self.spin_label)

        self.spin_point_size = QSpinBox()
        self.spin_point_size.setRange(self.point_size_min, self.point_size_max)
        self.spin_point_size.setValue(self.point_size)
        self.spin_point_size.valueChanged.connect(self.__set_point_size)
        self.addWidget(self.spin_point_size)

        self.text_editor_label = QLabel("Текст:")
        self.text_editor_label.setAlignment(Qt.AlignCenter)
        self.addWidget(self.text_editor_label)

        self.button_change_text = QPushButton("Изменить")
        self.button_change_text.setObjectName("ButtonNormal")
        self.button_change_text.setFixedSize(170, 32)
        self.button_change_text.clicked.connect(self.text_editor.show)

        self.addWidget(self.button_change_text)

        spacer = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)
        self.addItem(spacer)


class _ColorPalette(QVBoxLayout):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.color_list_white = ["white", "lightgray", "gray", "black", "magenta"]
        self.color_list_black = ["yellow", "red", "lime", "blue", "cyan"]
        self.color_list_buttons = []

        self.current_color = QColor("black")
        self.text_background_color = None
        self.text_back_color_prev = None

        self.red = 255
        self.green = 255
        self.blue = 255
        self.alpha = 255

        self.__setupUI()
        self.reset()

    def reset(self):
        self.text_background_color = None
        self.text_back_color_prev = None

        self.red = 255
        self.spinbox_red.setValue(255)
        self.slider_red.setValue(255)

        self.green = 255
        self.spinbox_green.setValue(255)
        self.slider_green.setValue(255)

        self.blue = 255
        self.spinbox_blue.setValue(255)
        self.slider_blue.setValue(255)

        self.alpha = 255
        self.spinbox_alpha.setValue(255)
        self.slider_alpha.setValue(255)

        self.current_color = QColor("black")
        self.text_indicator.set_color(self.current_color)
        self.text_indicator.reset()

    def __select_color(self):
        """Метод для установки текущего выбранного цвета."""
        color = self.sender().accessibleName()
        color = color.split("_")[1]
        self.current_color = QColor(color)
        self.text_indicator.set_color(self.current_color)

    def __set_rgba(self):
        self.current_color = QColor(self.red, self.green, self.blue, self.alpha)
        self.text_indicator.set_color(self.current_color)

    def __set_text_background_color(self):
        """Метод для установки цвета фона текста."""
        self.text_back_color_prev = self.text_background_color
        if self.text_back_color_prev == self.current_color:
            self.text_background_color = None
            self.text_indicator.set_back_color(self.text_background_color)
        else:
            self.text_background_color = self.current_color
            self.text_indicator.set_back_color(self.text_background_color)

    def __set_red(self):
        if self.spinbox_red.value() != self.red:
            self.red = self.spinbox_red.value()
        elif self.slider_red.value() != self.red:
            self.red = self.slider_red.value()

        self.spinbox_red.setValue(self.red)
        self.slider_red.setValue(self.red)
        self.__set_rgba()

    def __set_green(self):
        if self.spinbox_green.value() != self.green:
            self.green = self.spinbox_green.value()
        elif self.slider_green.value() != self.green:
            self.green = self.slider_green.value()

        self.spinbox_green.setValue(self.green)
        self.slider_green.setValue(self.green)
        self.__set_rgba()

    def __set_blue(self):
        if self.spinbox_blue.value() != self.blue:
            self.blue = self.spinbox_blue.value()
        elif self.slider_blue.value() != self.blue:
            self.blue = self.slider_blue.value()

        self.spinbox_blue.setValue(self.blue)
        self.slider_blue.setValue(self.blue)
        self.__set_rgba()

    def __set_alpha(self):
        if self.spinbox_alpha.value() != self.alpha:
            self.alpha = self.spinbox_alpha.value()
        elif self.slider_alpha.value() != self.alpha:
            self.alpha = self.slider_alpha.value()

        self.spinbox_alpha.setValue(self.alpha)
        self.spinbox_alpha.setValue(self.alpha)
        self.__set_rgba()

    def __setupUI(self):
        self.label_colors = QLabel("Цвета")
        self.label_colors.setAlignment(Qt.AlignCenter)
        self.addWidget(self.label_colors)

        self.VLayout_1 = QVBoxLayout()
        self.VLayout_2 = QVBoxLayout()
        self.HLayout = QHBoxLayout()
        self.HLayout.addLayout(self.VLayout_1)
        self.HLayout.addLayout(self.VLayout_2)

        self.addLayout(self.HLayout)

        for i in self.color_list_white:
            button = QPushButton()
            button.setObjectName("ButtonRoundColor")
            button.setAccessibleName(f"button_{i}")
            button.setStyleSheet(f"background-color: {i};")
            button.clicked.connect(self.__select_color)
            self.color_list_buttons.append(button)
            self.VLayout_1.addWidget(button)

        for i in self.color_list_black:
            button = QPushButton()
            button.setObjectName("ButtonRoundColor")
            button.setAccessibleName(f"button_{i}")
            button.setStyleSheet(f"background-color: {i};")
            button.clicked.connect(self.__select_color)
            self.color_list_buttons.append(button)
            self.VLayout_2.addWidget(button)

        self.label_colors_2 = QLabel("Пользовательский")
        self.label_colors_2.setAlignment(Qt.AlignCenter)
        self.addWidget(self.label_colors_2)

        self.label_red = QLabel("R")
        self.label_red.setObjectName("LabelRed")

        self.spinbox_red = QSpinBox()
        self.spinbox_red.setObjectName("SpinBoxSteals")
        self.spinbox_red.setRange(0, 255)
        self.spinbox_red.valueChanged.connect(self.__set_red)

        self.slider_red = QSlider(Qt.Horizontal)
        self.slider_red.setObjectName("SliderColor")
        self.slider_red.setRange(0, 255)
        self.slider_red.valueChanged.connect(self.__set_red)

        self.chanel_red = QHBoxLayout()
        self.chanel_red.addWidget(self.label_red)
        self.chanel_red.addWidget(self.spinbox_red)
        self.chanel_red.addWidget(self.slider_red)
        self.addLayout(self.chanel_red)

        self.label_green = QLabel("G")
        self.label_green.setObjectName("LabelGreen")

        self.spinbox_green = QSpinBox()
        self.spinbox_green.setObjectName("SpinBoxSteals")
        self.spinbox_green.setRange(0, 255)
        self.spinbox_green.valueChanged.connect(self.__set_green)

        self.slider_green = QSlider(Qt.Horizontal)
        self.slider_green.setObjectName("SliderColor")
        self.slider_green.setRange(0, 255)
        self.slider_green.valueChanged.connect(self.__set_green)

        self.chanel_green = QHBoxLayout()
        self.chanel_green.addWidget(self.label_green)
        self.chanel_green.addWidget(self.spinbox_green)
        self.chanel_green.addWidget(self.slider_green)
        self.addLayout(self.chanel_green)

        self.label_blue = QLabel("B")
        self.label_blue.setObjectName("LabelBlue")

        self.spinbox_blue = QSpinBox()
        self.spinbox_blue.setObjectName("SpinBoxSteals")
        self.spinbox_blue.setRange(0, 255)
        self.spinbox_blue.valueChanged.connect(self.__set_blue)

        self.slider_blue = QSlider(Qt.Horizontal)
        self.slider_blue.setObjectName("SliderColor")
        self.slider_blue.setRange(0, 255)
        self.slider_blue.valueChanged.connect(self.__set_blue)

        self.chanel_blue = QHBoxLayout()
        self.chanel_blue.addWidget(self.label_blue)
        self.chanel_blue.addWidget(self.spinbox_blue)
        self.chanel_blue.addWidget(self.slider_blue)
        self.addLayout(self.chanel_blue)

        self.label_alpha = QLabel("A")
        self.label_alpha.setObjectName("LabelAlpha")

        self.spinbox_alpha = QSpinBox()
        self.spinbox_alpha.setObjectName("SpinBoxSteals")
        self.spinbox_alpha.setRange(0, 255)
        self.spinbox_alpha.valueChanged.connect(self.__set_alpha)

        self.slider_alpha = QSlider(Qt.Horizontal)
        self.slider_alpha.setObjectName("SliderColor")
        self.slider_alpha.setRange(0, 255)
        self.slider_alpha.valueChanged.connect(self.__set_alpha)

        self.chanel_alpha = QHBoxLayout()
        self.chanel_alpha.addWidget(self.label_alpha)
        self.chanel_alpha.addWidget(self.spinbox_alpha)
        self.chanel_alpha.addWidget(self.slider_alpha)
        self.addLayout(self.chanel_alpha)

        self.text_indicator = _TextIndicator(self)
        self.text_indicator.clicked.connect(self.__set_text_background_color)
        self.addWidget(self.text_indicator)


class _TextIndicator(QPushButton):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.color = QColor("black")
        self.back_color = None
        self.back_color_prev = None

        self.setText("Пример\nТекста")

        self.__update()

    def reset(self):
        self.color = QColor("black")
        self.back_color = None
        self.back_color_prev = None
        self.__update()

    def __update(self):
        if self.back_color is None:
            self.setStyleSheet(f"color: {self.color.name()}; background-color: transparent;"
                               f"border: 2px solid black; border-radius: 25px;"
                               f"margin-bottom: 10px; padding: 5px 0px;"
                               f"width: 170; height: 50px;"
                               f"max-width: 170; max-height: 50px;")
        else:
            self.setStyleSheet(f"color: {self.color.name()}; background-color: {self.back_color.name()};"
                               f"border: 2px solid black; border-radius: 25px;"
                               f"margin-bottom: 10px; padding: 5px 0px;"
                               f"width: 170; height: 50px;"
                               f"max-width: 170; max-height: 50px;")

    def set_color(self, color: QColor):
        self.color = color
        self.__update()

    def set_back_color(self, color):
        if self.back_color_prev == color:
            self.back_color_prev = self.back_color
            self.back_color = None
        else:
            self.back_color_prev = self.back_color
            self.back_color = color
        self.__update()


class _TextEditor(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.text = ""
        self.setWindowTitle("Текст")
        self.__setupUI()

    def reset(self):
        self.text = ""
        self.text_editor.setText("")

    def __save(self):
        self.text = self.text_editor.toPlainText()
        self.close()

    def __cancel(self):
        self.text_editor.setText(self.text)
        self.close()

    def __setupUI(self):
        self.setWindowFlags(Qt.CustomizeWindowHint |
                            Qt.WindowTitleHint |
                            Qt.WindowShadeButtonHint |
                            Qt.Tool |
                            Qt.WindowStaysOnTopHint)

        self.VLayout = QVBoxLayout()
        self.HLayout = QHBoxLayout()

        self.text_editor = QTextEdit()

        self.button_save = QPushButton("Принять")
        self.button_save.setObjectName("ButtonOK")
        self.button_save.setFixedSize(170, 32)
        self.button_save.clicked.connect(self.__save)

        self.button_cancel = QPushButton("Отмена")
        self.button_cancel.setObjectName("ButtonCancel")
        self.button_cancel.setFixedSize(170, 32)
        self.button_cancel.clicked.connect(self.__cancel)

        self.VLayout.addWidget(self.text_editor)
        self.VLayout.addLayout(self.HLayout)
        self.HLayout.addWidget(self.button_save)
        self.HLayout.addWidget(self.button_cancel)
        self.setLayout(self.VLayout)


class _DrawLabel(QLabel):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.setObjectName("DrawLabel")
        self.resize(400, 400)
        self.setPixmap(QPixmap())

        self.pen = None
        self.draw_end = False
        self.point_start = None
        self.point_prev = None
        self.pixmap_show = None
        self.pixmap_last = None

    @property
    def tools_menu(self):
        return self.parent.tools_menu

    @property
    def tools(self):
        return self.parent.tools

    @property
    def colors(self):
        return self.parent.colors

    @property
    def current_tool(self):
        return self.tools_menu.current_tool

    @property
    def text(self):
        return self.tools.text

    @property
    def point_size(self):
        return self.tools.point_size

    @property
    def point_form(self):
        return self.tools.point_form

    @property
    def current_color(self):
        return self.colors.current_color

    @property
    def text_background_color(self):
        return self.colors.text_background_color

    def reset(self):
        self.resize(400, 400)
        self.pen = None
        self.draw_end = False
        self.point_start = None
        self.point_prev = None
        self.pixmap_show = None
        self.pixmap_last = None

    def set_pixmap(self, image_path: str):
        ext = image_path.split(".")[-1]
        trans_img = ["png", "tiff"]
        if ext not in trans_img:
            open_img = QPixmap(image_path)
            pixmap = QPixmap(open_img.size())
            pixmap.fill(Qt.transparent)
            painter = QPainter()
            painter.begin(pixmap)
            painter.drawPixmap(0, 0, open_img)
            painter.end()
        else:
            pixmap = QPixmap(image_path)

        self.pixmap_show = pixmap.copy()
        self.pixmap_last = pixmap.copy()
        self.setPixmap(self.pixmap_show)
        self.resize(pixmap.size())
        self.updateGeometry()
        self.update()

    def prepare_to_save(self, ext: str):
        trans_img = ["png", "tiff"]  # gif and bmp have bag
        if ext.lower() not in trans_img:
            store = self.pixmap_last.copy()
            self.pixmap_last.fill(Qt.white)
            p = QPainter(self.pixmap_last)
            p.drawPixmap(0, 0, store)
            p.end()

    def image_undo(self):
        """Метод для отмены последнего действия."""
        pass

    def mousePressEvent(self, e):
        """Переопределение события при нажании на ЛКМ и ПКМ."""
        if e.button() == Qt.RightButton:
            self.tools_menu.move(QCursor.pos().x(), QCursor.pos().y())
            self.tools_menu.show()
        elif e.button() == Qt.LeftButton:
            e.accept()
            pos = self.mapFromGlobal(QCursor.pos())  # QPoint

            self.point_start = pos
            self.point_prev = pos

            if self.current_tool == "Pen":
                self.draw_pen(pos, pos)
            elif self.current_tool == "Eraser":
                self.draw_eraser(pos, pos)
            elif self.current_tool == "Line":
                self.draw_line(pos, pos)
            elif self.current_tool == "Text":
                self.draw_text(pos, pos, self.text_background_color)

    def mouseMoveEvent(self, e):
        """Переопределение события на перемещение курсора с зажатым ЛКМ."""
        e.accept()
        pos = self.mapFromGlobal(QCursor.pos())
        if self.current_tool == "Pen":
            self.draw_pen(pos, self.point_prev)
        elif self.current_tool == "Eraser":
            self.draw_eraser(pos, self.point_prev)
        elif self.current_tool == "Line":
            self.draw_line(self.point_start, pos)
        elif self.current_tool == "Text":
            self.draw_text(self.point_start, pos, self.text_background_color)

        self.point_prev = pos

    def mouseReleaseEvent(self, e):
        """Переопределение события при отпускании ЛКМ."""
        e.accept()
        pos = self.mapFromGlobal(QCursor.pos())
        self.draw_end = True

        if self.current_tool == "Pen":
            self.draw_pen(pos, self.point_prev)
        elif self.current_tool == "Eraser":
            self.draw_eraser(pos, self.point_prev)
        elif self.current_tool == "Line":
            self.draw_line(self.point_start, pos)
        elif self.current_tool == "Text":
            self.draw_text(self.point_start, pos, self.text_background_color)

        self.point_start = None
        self.point_prev = None
        self.draw_end = False

    def draw_pen(self, pos: QPoint, prev: QPoint):
        """Метод для отрисовки точек инструментом Карандаш."""
        painter = QPainter()
        painter.begin(self.pixmap_show)
        self.__set_painter(painter)
        if prev != pos and prev is not None:
            painter.drawLine(prev, pos)
        elif prev == pos:
            painter.drawPoint(pos)
        painter.end()

        self.pixmap_last = self.pixmap_show.copy()
        self.setPixmap(self.pixmap_show)

    def draw_eraser(self, pos: QPoint, prev: QPoint):
        """Метод для отрисовки точек инструментом Ластик."""
        painter = QPainter()
        painter.begin(self.pixmap_show)
        self.__set_eraser(painter)
        if prev != pos and prev is not None:
            painter.drawLine(prev, pos)
        elif prev == pos:
            painter.drawPoint(pos)
        painter.end()

        self.pixmap_last = self.pixmap_show.copy()
        self.setPixmap(self.pixmap_show)

    def draw_line(self, start: QPoint, end: QPoint):
        """Метод для рисовании на изображении прямых линий."""
        self.setPixmap(self.pixmap_last)
        self.pixmap_show = self.pixmap_last.copy()

        painter = QPainter()
        painter.begin(self.pixmap_show)
        self.__set_painter(painter)
        painter.drawLine(start, end)
        painter.end()

        self.setPixmap(self.pixmap_show)
        if self.draw_end:
            self.pixmap_last = self.pixmap_show.copy()

    def draw_text(self, start: QPoint, end: QPoint, back_color):
        """Метод для помещения текста на изображении."""
        self.setPixmap(self.pixmap_last)
        self.pixmap_show = self.pixmap_last.copy()

        painter = QPainter()
        painter.begin(self.pixmap_show)
        self.__set_painter(painter)

        rect = QRect(start, end)
        if back_color is not None:
            painter.fillRect(rect, back_color)

        painter.setFont(QFont("Times", self.point_size))
        painter.drawText(rect, Qt.AlignCenter, self.text)
        painter.end()

        self.setPixmap(self.pixmap_show)

        if self.draw_end:
            self.pixmap_last = self.pixmap_show.copy()

    def __set_painter(self, painter: QPainter):
        self.pen = QPen(self.current_color, self.point_size, Qt.SolidLine, self.point_form, Qt.RoundJoin)
        painter.setPen(self.pen)
        painter.setRenderHint(QPainter.SmoothPixmapTransform, True)
        painter.setRenderHint(QPainter.TextAntialiasing, True)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setRenderHint(QPainter.LosslessImageRendering, True)

    def __set_eraser(self, painter: QPainter):
        self.__set_painter(painter)
        painter.setCompositionMode(QPainter.CompositionMode_Clear)
