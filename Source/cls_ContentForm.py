from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import QSize, pyqtSignal
from PyQt5.QtGui import QPixmap

from Source.UI_ContentForm import Ui_ContentForm


class ContentForm(QWidget, Ui_ContentForm):
    """docstring for ContentForm."""
    maxWidth = 320
    open_info = pyqtSignal()
    search = pyqtSignal()
    coppy_buffer = pyqtSignal()

    def __init__(self, parent, object_namedtuple):
        # if (type(object_namedtuple) != "namedtuple"):
        #     print("Error, object_namedtuple is not namedtuple")
        #     return None
        # print(object_namedtuple)
        QWidget.__init__(self, parent)
        self.setupUi(self)
        self.object_namedtuple = object_namedtuple
        self.search_tag = None

        self.Title.setText(self.object_namedtuple.Title.capitalize())
        self.scrollArea.add_tags(self.object_namedtuple.Tags, 2)
        self.set_picture()
        self.Picture.clicked.connect(self._open_info)
        self.BuferCopy.clicked.connect(self._coppy_buffer)
        self.scrollArea.search.connect(self._search)

    def set_picture(self):
        if self.object_namedtuple.TIPath != "None":
            picture = QPixmap(self.object_namedtuple.TIPath)
            try:
                height = 330 * (picture.height() / picture.width())
            except Exception as e:
                height = 330
            self.Picture.setMinimumSize(QSize(330, height))
            self.Picture.setMaximumSize(QSize(330, height))
            self.Picture.setPixmap(picture)
    #
    # def clearLayout(self, layout):
    #     # print(layout.count())
    #     while layout.count():
    #         child = layout.takeAt(0)
    #         print(type(child))
    #         if child.layout():
    #             self.clearLayout(child)
    #             child.layout().deleteLater()
    #         if child.widget():
    #             child.widget().deleteLater()
    #     # print(layout.count())
    #
    # def deleteLater(self):
    #     self.scrollArea.clear()
    #     self.clearLayout(self.verticalLayout)
    #     super().deleteLater()

    def _open_info(self):
        self.open_info.emit()

    def _search(self):
        self.search_tag = self.sender().search_tag
        self.search.emit()

    def _coppy_buffer(self):
        self.coppy_buffer.emit()
