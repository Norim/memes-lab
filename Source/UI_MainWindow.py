# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Main_Window.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MemesLab(object):
    def setupUi(self, MemesLab):
        MemesLab.setObjectName("MemesLab")
        MemesLab.resize(1280, 721)
        MemesLab.setMinimumSize(QtCore.QSize(1280, 720))
        MemesLab.setStyleSheet("background-color: rgb(24, 25, 29);")
        self.centralwidget.setStyleSheet("color: rgb(229, 229, 229);")
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.Top_Wiget = QtWidgets.QWidget(self.centralwidget)
        self.Top_Wiget.setMinimumSize(QtCore.QSize(1100, 60))
        self.Top_Wiget.setMaximumSize(QtCore.QSize(16777215, 61))
        self.Top_Wiget.setStyleSheet("background-color: rgb(40, 46, 51);\n"
"font: 87 13pt \"Arial\";")
        self.Top_Wiget.setObjectName("Top_Wiget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.Top_Wiget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 38, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem)
        self._Search_String = QtWidgets.QWidget(self.Top_Wiget)
        self._Search_String.setMinimumSize(QtCore.QSize(100, 30))
        self._Search_String.setMaximumSize(QtCore.QSize(1000, 35))
        self._Search_String.setStyleSheet("background-color: rgb(61, 68, 75);\n"
"border-color: rgb(255, 0, 0);\n"
"border-radius: 8px;")
        self._Search_String.setObjectName("_Search_String")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self._Search_String)
        self.verticalLayout_3.setContentsMargins(-1, 0, -1, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.Search_String = QLineEditCompleter(self._Search_String)
        self.Search_String.setMinimumSize(QtCore.QSize(500, 30))
        self.Search_String.setMaximumSize(QtCore.QSize(1000, 30))
        self.Search_String.setToolTip("")
        self.Search_String.setAutoFillBackground(False)
        self.Search_String.setStyleSheet("")
        self.Search_String.setText("")
        self.Search_String.setFrame(True)
        self.Search_String.setClearButtonEnabled(False)
        self.Search_String.setObjectName("Search_String")
        self.verticalLayout_3.addWidget(self.Search_String)
        self.horizontalLayout.addWidget(self._Search_String)
        spacerItem1 = QtWidgets.QSpacerItem(20, 38, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem1)
        self.Group_Select = QComboBoxCompleter(self.Top_Wiget)
        self.Group_Select.setMinimumSize(QtCore.QSize(150, 35))
        self.Group_Select.setStyleSheet("background-color: rgb(61, 68, 75);")
        self.Group_Select.setObjectName("Group_Select")
        self.horizontalLayout.addWidget(self.Group_Select)
        self.Format_Select = QComboBoxCompleter(self.Top_Wiget)
        self.Format_Select.setMinimumSize(QtCore.QSize(150, 35))
        self.Format_Select.setStyleSheet("background-color: rgb(61, 68, 75);\n"
"")
        self.Format_Select.setObjectName("Format_Select")
        self.horizontalLayout.addWidget(self.Format_Select)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem2)
        self.Search_Button = QPushButtonStyleFirst(self.Top_Wiget)
        self.Search_Button.setMinimumSize(QtCore.QSize(100, 35))
        self.Search_Button.setStyleSheet("background-color: rgb(0, 150, 135);\n"
"border-radius: 8px;")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/Search.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Search_Button.setIcon(icon)
        self.Search_Button.setIconSize(QtCore.QSize(20, 20))
        self.Search_Button.setFlat(False)
        self.Search_Button.setObjectName("Search_Button")
        self.horizontalLayout.addWidget(self.Search_Button)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem3)
        self.gridLayout.addWidget(self.Top_Wiget, 0, 1, 1, 1)
        self._MemesLab = QtWidgets.QWidget(self.centralwidget)
        self._MemesLab.setMinimumSize(QtCore.QSize(180, 60))
        self._MemesLab.setMaximumSize(QtCore.QSize(180, 61))
        self._MemesLab.setStyleSheet("background-color: rgb(40, 46, 51);")
        self._MemesLab.setObjectName("_MemesLab")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self._MemesLab)
        self.horizontalLayout_2.setContentsMargins(10, 3, 5, 3)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self._Icon_MemesLab = QtWidgets.QLabel(self._MemesLab)
        self._Icon_MemesLab.setMinimumSize(QtCore.QSize(50, 50))
        self._Icon_MemesLab.setText("")
        self._Icon_MemesLab.setPixmap(QtGui.QPixmap(":/icons/Kanna.png"))
        self._Icon_MemesLab.setScaledContents(True)
        self._Icon_MemesLab.setObjectName("_Icon_MemesLab")
        self.horizontalLayout_2.addWidget(self._Icon_MemesLab, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self._Text_MemesLab = QtWidgets.QLabel(self._MemesLab)
        self._Text_MemesLab.setMinimumSize(QtCore.QSize(100, 0))
        self._Text_MemesLab.setStyleSheet("font: 87 13pt \"Arial\";")
        self._Text_MemesLab.setObjectName("_Text_MemesLab")
        self.horizontalLayout_2.addWidget(self._Text_MemesLab)
        self.gridLayout.addWidget(self._MemesLab, 0, 0, 1, 1)
        self._Menu_Wiget = QtWidgets.QWidget(self.centralwidget)
        self._Menu_Wiget.setMinimumSize(QtCore.QSize(180, 631))
        self._Menu_Wiget.setMaximumSize(QtCore.QSize(180, 16777215))
        font = QtGui.QFont()
        font.setFamily("Futura PT Medium")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self._Menu_Wiget.setFont(font)
        self._Menu_Wiget.setStyleSheet("background-color: rgb(40, 46, 51);\n"
"font: 57 14pt \"Futura PT Medium\";\n"
"")
        self._Menu_Wiget.setObjectName("_Menu_Wiget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self._Menu_Wiget)
        self.verticalLayout.setContentsMargins(0, -1, 0, 10)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self._line_top = QtWidgets.QFrame(self._Menu_Wiget)
        self._line_top.setFrameShape(QtWidgets.QFrame.HLine)
        self._line_top.setFrameShadow(QtWidgets.QFrame.Sunken)
        self._line_top.setObjectName("_line_top")
        self.verticalLayout.addWidget(self._line_top)
        self.Home_Button = QPushButtonMenu(self._Menu_Wiget)
        self.Home_Button.setEnabled(True)
        self.Home_Button.setMinimumSize(QtCore.QSize(0, 50))
        font = QtGui.QFont()
        font.setFamily("Futura PT Medium")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.Home_Button.setFont(font)
        self.Home_Button.setAcceptDrops(False)
        self.Home_Button.setAccessibleDescription("")
        self.Home_Button.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.Home_Button.setAutoFillBackground(False)
        self.Home_Button.setStyleSheet("background-color: rgb(61, 68, 75);")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons/Home.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Home_Button.setIcon(icon1)
        self.Home_Button.setIconSize(QtCore.QSize(36, 36))
        self.Home_Button.setAutoRepeat(False)
        self.Home_Button.setAutoExclusive(False)
        self.Home_Button.setAutoRepeatDelay(300)
        self.Home_Button.setAutoRepeatInterval(100)
        self.Home_Button.setAutoDefault(False)
        self.Home_Button.setDefault(False)
        self.Home_Button.setFlat(False)
        self.Home_Button.setObjectName("Home_Button")
        self.verticalLayout.addWidget(self.Home_Button)
        self.Add_Button = QPushButtonMenu(self._Menu_Wiget)
        self.Add_Button.setEnabled(True)
        self.Add_Button.setMinimumSize(QtCore.QSize(0, 50))
        self.Add_Button.setStyleSheet("gridline-color: rgb(255, 255, 255);")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons/Add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Add_Button.setIcon(icon2)
        self.Add_Button.setIconSize(QtCore.QSize(36, 36))
        self.Add_Button.setFlat(True)
        self.Add_Button.setObjectName("Add_Button")
        self.verticalLayout.addWidget(self.Add_Button, 0, QtCore.Qt.AlignTop)
        self._line = QtWidgets.QFrame(self._Menu_Wiget)
        self._line.setFrameShape(QtWidgets.QFrame.HLine)
        self._line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self._line.setObjectName("_line")
        self.verticalLayout.addWidget(self._line)
        spacerItem4 = QtWidgets.QSpacerItem(40, 60, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem4)
        self.Time_Lapse_Button = QPushButtonMenu(self._Menu_Wiget)
        self.Time_Lapse_Button.setEnabled(True)
        self.Time_Lapse_Button.setMinimumSize(QtCore.QSize(0, 50))
        self.Time_Lapse_Button.setStyleSheet("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/icons/List.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Time_Lapse_Button.setIcon(icon3)
        self.Time_Lapse_Button.setIconSize(QtCore.QSize(36, 36))
        self.Time_Lapse_Button.setFlat(True)
        self.Time_Lapse_Button.setObjectName("Time_Lapse_Button")
        self.verticalLayout.addWidget(self.Time_Lapse_Button, 0, QtCore.Qt.AlignVCenter)
        self.List_Button = QPushButtonMenu(self._Menu_Wiget)
        self.List_Button.setEnabled(True)
        self.List_Button.setMinimumSize(QtCore.QSize(0, 50))
        self.List_Button.setIcon(icon3)
        self.List_Button.setIconSize(QtCore.QSize(36, 36))
        self.List_Button.setFlat(True)
        self.List_Button.setObjectName("List_Button")
        self.verticalLayout.addWidget(self.List_Button, 0, QtCore.Qt.AlignVCenter)
        self.Differed_Button = QPushButtonMenu(self._Menu_Wiget)
        self.Differed_Button.setEnabled(True)
        self.Differed_Button.setMinimumSize(QtCore.QSize(0, 50))
        self.Differed_Button.setStyleSheet("")
        self.Differed_Button.setIcon(icon3)
        self.Differed_Button.setIconSize(QtCore.QSize(36, 36))
        self.Differed_Button.setFlat(True)
        self.Differed_Button.setObjectName("Differed_Button")
        self.verticalLayout.addWidget(self.Differed_Button, 0, QtCore.Qt.AlignVCenter)
        spacerItem5 = QtWidgets.QSpacerItem(148, 1000, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem5)
        self.Settings_Button = QPushButtonMenu(self._Menu_Wiget)
        self.Settings_Button.setEnabled(True)
        self.Settings_Button.setMinimumSize(QtCore.QSize(0, 50))
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/icons/Settings.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Settings_Button.setIcon(icon4)
        self.Settings_Button.setIconSize(QtCore.QSize(32, 32))
        self.Settings_Button.setFlat(True)
        self.Settings_Button.setObjectName("Settings_Button")
        self.verticalLayout.addWidget(self.Settings_Button, 0, QtCore.Qt.AlignBottom)
        self.gridLayout.addWidget(self._Menu_Wiget, 1, 0, 1, 1)
        self.Main = QtWidgets.QStackedWidget(self.centralwidget)
        self.Main.setStyleSheet("background-color: rgb(24, 25, 29);")
        self.Main.setObjectName("Main")
        self.Home_Page = QtWidgets.QWidget()
        self.Home_Page.setStyleSheet("font: 87 12pt \"Arial\";")
        self.Home_Page.setObjectName("Home_Page")
        self.verticalLayout_10 = QtWidgets.QVBoxLayout(self.Home_Page)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.label = QtWidgets.QLabel(self.Home_Page)
        self.label.setObjectName("label")
        self.verticalLayout_10.addWidget(self.label)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.Home_Page_Menu = QtWidgets.QWidget(self.Home_Page)
        self.Home_Page_Menu.setStyleSheet("font: 87 15pt \"Arial\";")
        self.Home_Page_Menu.setObjectName("Home_Page_Menu")
        self.formLayout = QtWidgets.QFormLayout(self.Home_Page_Menu)
        self.formLayout.setFormAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.formLayout.setContentsMargins(25, 100, -1, -1)
        self.formLayout.setSpacing(25)
        self.formLayout.setObjectName("formLayout")
        self.Home_Picture_1 = QtWidgets.QLabel(self.Home_Page_Menu)
        self.Home_Picture_1.setMaximumSize(QtCore.QSize(40, 40))
        self.Home_Picture_1.setText("")
        self.Home_Picture_1.setPixmap(QtGui.QPixmap(":/icons/Folder.png"))
        self.Home_Picture_1.setScaledContents(True)
        self.Home_Picture_1.setObjectName("Home_Picture_1")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.Home_Picture_1)
        self.From_Folder = QLabelUnderLine(self.Home_Page_Menu)
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(15)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(10)
        self.From_Folder.setFont(font)
        self.From_Folder.setObjectName("From_Folder")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.From_Folder)
        self.horizontalLayout_6.addWidget(self.Home_Page_Menu)
        self.Kana_Picture = QtWidgets.QLabel(self.Home_Page)
        self.Kana_Picture.setMaximumSize(QtCore.QSize(391, 563))
        self.Kana_Picture.setText("")
        self.Kana_Picture.setPixmap(QtGui.QPixmap(":/icons/Kanna_Big.png"))
        self.Kana_Picture.setScaledContents(True)
        self.Kana_Picture.setObjectName("Kana_Picture")
        self.horizontalLayout_6.addWidget(self.Kana_Picture)
        self.verticalLayout_10.addLayout(self.horizontalLayout_6)
        self.Main.addWidget(self.Home_Page)
        self._List_Page = QtWidgets.QWidget()
        self._List_Page.setObjectName("_List_Page")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self._List_Page)
        self.verticalLayout_4.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.verticalLayout_4.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.List_Page_ScrollArea = QScrollAreaSpace(self._List_Page)
        self.List_Page_ScrollArea.setStyleSheet("border-radius: 1px;")
        self.List_Page_ScrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.List_Page_ScrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.List_Page_ScrollArea.setWidgetResizable(True)
        self.List_Page_ScrollArea.setAlignment(QtCore.Qt.AlignCenter)
        self.List_Page_ScrollArea.setObjectName("List_Page_ScrollArea")
        self.List_Page = QtWidgets.QWidget()
        self.List_Page.setGeometry(QtCore.QRect(0, 0, 1090, 651))
        self.List_Page.setStyleSheet("")
        self.List_Page.setObjectName("List_Page")
        self.List_Page_Latout = QtWidgets.QVBoxLayout(self.List_Page)
        self.List_Page_Latout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.List_Page_Latout.setContentsMargins(0, 0, 0, 0)
        self.List_Page_Latout.setSpacing(10)
        self.List_Page_Latout.setObjectName("List_Page_Latout")
        self._List_Title = QtWidgets.QWidget(self.List_Page)
        self._List_Title.setMaximumSize(QtCore.QSize(16777215, 80))
        self._List_Title.setObjectName("_List_Title")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self._List_Title)
        self.horizontalLayout_4.setContentsMargins(50, 0, 50, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.List_Title = QtWidgets.QLabel(self._List_Title)
        self.List_Title.setMaximumSize(QtCore.QSize(16777215, 70))
        self.List_Title.setStyleSheet("font: 87 32pt \"Arial\";")
        self.List_Title.setTextFormat(QtCore.Qt.AutoText)
        self.List_Title.setScaledContents(False)
        self.List_Title.setObjectName("List_Title")
        self.horizontalLayout_4.addWidget(self.List_Title)
        self.List_Page_Latout.addWidget(self._List_Title)
        self.Sortet_Lane = QtWidgets.QWidget(self.List_Page)
        self.Sortet_Lane.setMaximumSize(QtCore.QSize(16777215, 60))
        self.Sortet_Lane.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.Sortet_Lane.setAutoFillBackground(False)
        self.Sortet_Lane.setStyleSheet("font: 87 21pt \"Arial\";\n"
"border-width: 4px;\n"
"border-bottom-color: rgb(0, 150, 135);")
        self.Sortet_Lane.setObjectName("Sortet_Lane")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.Sortet_Lane)
        self.horizontalLayout_3.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.horizontalLayout_3.setContentsMargins(-1, 5, 30, 5)
        self.horizontalLayout_3.setSpacing(10)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.Popular_Sorted = QLabelHighLight(self.Sortet_Lane)
        self.Popular_Sorted.setMinimumSize(QtCore.QSize(230, 0))
        self.Popular_Sorted.setMaximumSize(QtCore.QSize(250, 16777215))
        self.Popular_Sorted.setStyleSheet("")
        self.Popular_Sorted.setAlignment(QtCore.Qt.AlignCenter)
        self.Popular_Sorted.setObjectName("Popular_Sorted")
        self.horizontalLayout_3.addWidget(self.Popular_Sorted)
        self.New_Sorted = QLabelHighLight(self.Sortet_Lane)
        self.New_Sorted.setMinimumSize(QtCore.QSize(180, 0))
        self.New_Sorted.setMaximumSize(QtCore.QSize(200, 16777215))
        self.New_Sorted.setStyleSheet("")
        self.New_Sorted.setAlignment(QtCore.Qt.AlignCenter)
        self.New_Sorted.setObjectName("New_Sorted")
        self.horizontalLayout_3.addWidget(self.New_Sorted)
        self.Old_Sorted = QLabelHighLight(self.Sortet_Lane)
        self.Old_Sorted.setMinimumSize(QtCore.QSize(150, 0))
        self.Old_Sorted.setMaximumSize(QtCore.QSize(160, 16777215))
        self.Old_Sorted.setStyleSheet("")
        self.Old_Sorted.setAlignment(QtCore.Qt.AlignCenter)
        self.Old_Sorted.setObjectName("Old_Sorted")
        self.horizontalLayout_3.addWidget(self.Old_Sorted)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem6)
        self.Mixer = QLabelClickedStyleFirst(self.Sortet_Lane)
        self.Mixer.setMaximumSize(QtCore.QSize(32, 32))
        self.Mixer.setText("")
        self.Mixer.setPixmap(QtGui.QPixmap(":/icons/Rechange.png"))
        self.Mixer.setScaledContents(True)
        self.Mixer.setObjectName("Mixer")
        self.horizontalLayout_3.addWidget(self.Mixer)
        self.List_Page_Latout.addWidget(self.Sortet_Lane)
        self.Desk_ContentForm = QtWidgets.QWidget(self.List_Page)
        self.Desk_ContentForm.setAutoFillBackground(False)
        self.Desk_ContentForm.setStyleSheet("")
        self.Desk_ContentForm.setObjectName("Desk_ContentForm")
        self.Gallery_Layout = QtWidgets.QHBoxLayout(self.Desk_ContentForm)
        self.Gallery_Layout.setContentsMargins(0, 15, 0, 0)
        self.Gallery_Layout.setSpacing(0)
        self.Gallery_Layout.setObjectName("Gallery_Layout")
        self.List_Page_Latout.addWidget(self.Desk_ContentForm)
        self.List_Page_ScrollArea.setWidget(self.List_Page)
        self.verticalLayout_4.addWidget(self.List_Page_ScrollArea)
        self.Main.addWidget(self._List_Page)
        self.Redactor_Page = QtWidgets.QWidget()
        self.Redactor_Page.setStyleSheet("font: 87 14pt \"Arial\";")
        self.Redactor_Page.setObjectName("Redactor_Page")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.Redactor_Page)
        self.gridLayout_2.setHorizontalSpacing(10)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.RedactorWidget = QtWidgets.QStackedWidget(self.Redactor_Page)
        self.RedactorWidget.setStyleSheet("")
        self.RedactorWidget.setObjectName("RedactorWidget")
        self.Meme_Page = QtWidgets.QWidget()
        self.Meme_Page.setObjectName("Meme_Page")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.Meme_Page)
        self.gridLayout_5.setContentsMargins(-1, 6, -1, -1)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self._Redactor_Head = QtWidgets.QWidget(self.Meme_Page)
        self._Redactor_Head.setMinimumSize(QtCore.QSize(1060, 91))
        self._Redactor_Head.setMaximumSize(QtCore.QSize(16777215, 91))
        self._Redactor_Head.setObjectName("_Redactor_Head")
        self._Top_Layout = QtWidgets.QHBoxLayout(self._Redactor_Head)
        self._Top_Layout.setContentsMargins(20, -1, 10, -1)
        self._Top_Layout.setSpacing(10)
        self._Top_Layout.setObjectName("_Top_Layout")
        self._Meme_Title_Bar = QtWidgets.QWidget(self._Redactor_Head)
        self._Meme_Title_Bar.setMinimumSize(QtCore.QSize(500, 60))
        self._Meme_Title_Bar.setMaximumSize(QtCore.QSize(16777215, 60))
        self._Meme_Title_Bar.setObjectName("_Meme_Title_Bar")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self._Meme_Title_Bar)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.Meme_Title = QtWidgets.QLineEdit(self._Meme_Title_Bar)
        self.Meme_Title.setStyleSheet("border-radius: 1px;\n"
"font: 20pt \"Arial\";")
        self.Meme_Title.setText("")
        self.Meme_Title.setReadOnly(False)
        self.Meme_Title.setObjectName("Meme_Title")
        self.verticalLayout_2.addWidget(self.Meme_Title)
        self._line_2 = QtWidgets.QFrame(self._Meme_Title_Bar)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self._line_2.sizePolicy().hasHeightForWidth())
        self._line_2.setSizePolicy(sizePolicy)
        self._line_2.setMinimumSize(QtCore.QSize(10, 1))
        self._line_2.setAutoFillBackground(False)
        self._line_2.setStyleSheet("background-color: rgb(229, 229, 229);;")
        self._line_2.setLineWidth(0)
        self._line_2.setMidLineWidth(10)
        self._line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self._line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self._line_2.setObjectName("_line_2")
        self.verticalLayout_2.addWidget(self._line_2)
        self._Top_Layout.addWidget(self._Meme_Title_Bar)
        spacerItem7 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self._Top_Layout.addItem(spacerItem7)
        self.__widget_2 = QtWidgets.QWidget(self._Redactor_Head)
        self.__widget_2.setMinimumSize(QtCore.QSize(40, 0))
        self.__widget_2.setObjectName("__widget_2")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.__widget_2)
        self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_9.setSpacing(10)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.Meme_Redacting = QComboBoxCompleter(self.__widget_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Meme_Redacting.sizePolicy().hasHeightForWidth())
        self.Meme_Redacting.setSizePolicy(sizePolicy)
        self.Meme_Redacting.setMinimumSize(QtCore.QSize(190, 30))
        self.Meme_Redacting.setMaximumSize(QtCore.QSize(190, 30))
        self.Meme_Redacting.setStyleSheet("background-color: rgb(61, 68, 75);")
        self.Meme_Redacting.setObjectName("Meme_Redacting")
        self.Meme_Redacting.addItem("")
        self.Meme_Redacting.addItem("")
        self.verticalLayout_9.addWidget(self.Meme_Redacting)
        self.Change_Picture = QPushButtonStyleFirst(self.__widget_2)
        self.Change_Picture.setMinimumSize(QtCore.QSize(0, 30))
        self.Change_Picture.setStyleSheet("border-radius: 8px; background-color: rgb(0, 150, 135);")
        self.Change_Picture.setObjectName("Change_Picture")
        self.verticalLayout_9.addWidget(self.Change_Picture)
        self._Top_Layout.addWidget(self.__widget_2)
        spacerItem8 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self._Top_Layout.addItem(spacerItem8)
        self.gridLayout_5.addWidget(self._Redactor_Head, 0, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.Meme_Picture = QWidgetPictureScaled(self.Meme_Page)
        self.Meme_Picture.setMinimumSize(QtCore.QSize(750, 400))
        self.Meme_Picture.setAcceptDrops(True)
        self.Meme_Picture.setObjectName("Meme_Picture")
        self.gridLayout_4.addWidget(self.Meme_Picture, 0, 0, 1, 1)
        self._widget_2 = QtWidgets.QWidget(self.Meme_Page)
        self._widget_2.setMinimumSize(QtCore.QSize(266, 400))
        self._widget_2.setMaximumSize(QtCore.QSize(400, 16777215))
        self._widget_2.setObjectName("_widget_2")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self._widget_2)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.Meme_Group_String = QLineEditCompleter(self._widget_2)
        self.Meme_Group_String.setMinimumSize(QtCore.QSize(0, 30))
        self.Meme_Group_String.setMaximumSize(QtCore.QSize(500, 30))
        self.Meme_Group_String.setToolTip("")
        self.Meme_Group_String.setAutoFillBackground(False)
        self.Meme_Group_String.setStyleSheet("background-color: rgb(61, 68, 75);\n"
"border-color: rgb(255, 0, 0);\n"
"border-radius: 8px;")
        self.Meme_Group_String.setText("")
        self.Meme_Group_String.setFrame(True)
        self.Meme_Group_String.setAlignment(QtCore.Qt.AlignCenter)
        self.Meme_Group_String.setObjectName("Meme_Group_String")
        self.verticalLayout_7.addWidget(self.Meme_Group_String)
        self.Meme_Description_String = QtWidgets.QTextEdit(self._widget_2)
        self.Meme_Description_String.setStyleSheet("background-color: rgb(35, 35, 36);\n"
"border-color: rgb(255, 0, 0);\n"
"border-radius: 8px;")
        self.Meme_Description_String.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.Meme_Description_String.setObjectName("Meme_Description_String")
        self.verticalLayout_7.addWidget(self.Meme_Description_String)
        self.gridLayout_4.addWidget(self._widget_2, 0, 2, 1, 1)
        spacerItem9 = QtWidgets.QSpacerItem(20, 5000, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_4.addItem(spacerItem9, 0, 1, 1, 1)
        spacerItem10 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_4.addItem(spacerItem10, 1, 1, 1, 1)
        self._Description_Bar = QtWidgets.QWidget(self.Meme_Page)
        self._Description_Bar.setMinimumSize(QtCore.QSize(760, 110))
        self._Description_Bar.setMaximumSize(QtCore.QSize(10000, 110))
        self._Description_Bar.setStyleSheet("\n"
"border-color: rgb(255, 0, 0);\n"
"color: rgb(229, 229, 229);\n"
"border-radius: 8px;")
        self._Description_Bar.setObjectName("_Description_Bar")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self._Description_Bar)
        self.verticalLayout_8.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.verticalLayout_8.setContentsMargins(10, -1, -1, -1)
        self.verticalLayout_8.setSpacing(10)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.Meme_scrollArea = QScrollAreaHorizontalTag(self._Description_Bar)
        self.Meme_scrollArea.setMaximumSize(QtCore.QSize(16777215, 40))
        self.Meme_scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.Meme_scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.Meme_scrollArea.setWidgetResizable(True)
        self.Meme_scrollArea.setObjectName("Meme_scrollArea")
        self.verticalLayout_8.addWidget(self.Meme_scrollArea)
        self._Meme_Tag_Bar = QtWidgets.QWidget(self._Description_Bar)
        self._Meme_Tag_Bar.setMinimumSize(QtCore.QSize(740, 40))
        self._Meme_Tag_Bar.setMaximumSize(QtCore.QSize(10000000, 40))
        self._Meme_Tag_Bar.setStyleSheet("background-color: rgb(35, 35, 36);\n"
"border-color: rgb(255, 0, 0);\n"
"border-radius: 8px;")
        self._Meme_Tag_Bar.setObjectName("_Meme_Tag_Bar")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self._Meme_Tag_Bar)
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.widget = QtWidgets.QWidget(self._Meme_Tag_Bar)
        self.widget.setMinimumSize(QtCore.QSize(200, 35))
        self.widget.setMaximumSize(QtCore.QSize(2000, 35))
        self.widget.setStyleSheet("background-color: rgb(61, 68, 75);\n"
"border-color: rgb(255, 0, 0);")
        self.widget.setObjectName("widget")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout_5.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_5.setSpacing(5)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setMaximumSize(QtCore.QSize(15, 30))
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_5.addWidget(self.label_3)
        self.Meme_Tag_String = QLineEditCompleter(self.widget)
        self.Meme_Tag_String.setMinimumSize(QtCore.QSize(30, 30))
        self.Meme_Tag_String.setMaximumSize(QtCore.QSize(2000, 30))
        self.Meme_Tag_String.setToolTip("")
        self.Meme_Tag_String.setAutoFillBackground(False)
        self.Meme_Tag_String.setStyleSheet("background-color: rgb(61, 68, 75);\n"
"border-color: rgb(255, 0, 0);\n"
"color: rgb(229, 229, 229);\n"
"border-radius: 8px;")
        self.Meme_Tag_String.setText("")
        self.Meme_Tag_String.setFrame(True)
        self.Meme_Tag_String.setObjectName("Meme_Tag_String")
        self.horizontalLayout_5.addWidget(self.Meme_Tag_String)
        self.horizontalLayout_7.addWidget(self.widget)
        spacerItem11 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout_7.addItem(spacerItem11)
        self.Meme_Add_Tag = QPushButtonStyleFirst(self._Meme_Tag_Bar)
        self.Meme_Add_Tag.setMinimumSize(QtCore.QSize(150, 35))
        self.Meme_Add_Tag.setMaximumSize(QtCore.QSize(150, 35))
        self.Meme_Add_Tag.setStyleSheet("background-color: rgb(0, 150, 135);")
        self.Meme_Add_Tag.setObjectName("Meme_Add_Tag")
        self.horizontalLayout_7.addWidget(self.Meme_Add_Tag)
        self.verticalLayout_8.addWidget(self._Meme_Tag_Bar)
        self.gridLayout_4.addWidget(self._Description_Bar, 1, 0, 1, 1)
        self._Button_Bar = QtWidgets.QWidget(self.Meme_Page)
        self._Button_Bar.setMinimumSize(QtCore.QSize(266, 110))
        self._Button_Bar.setMaximumSize(QtCore.QSize(400, 110))
        self._Button_Bar.setBaseSize(QtCore.QSize(250, 0))
        self._Button_Bar.setStyleSheet("border-radius: 8px;")
        self._Button_Bar.setObjectName("_Button_Bar")
        self.gridLayout_3 = QtWidgets.QGridLayout(self._Button_Bar)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.Delete_Button = QPushButtonStyleFirst(self._Button_Bar)
        self.Delete_Button.setMinimumSize(QtCore.QSize(90, 30))
        self.Delete_Button.setStyleSheet("background-color: rgb(80, 22, 22);")
        self.Delete_Button.setObjectName("Delete_Button")
        self.gridLayout_3.addWidget(self.Delete_Button, 1, 0, 1, 1)
        self.Copy_Button = QPushButtonStyleFirst(self._Button_Bar)
        self.Copy_Button.setMinimumSize(QtCore.QSize(0, 30))
        self.Copy_Button.setStyleSheet("background-color:  rgb(40, 46, 51);")
        self.Copy_Button.setObjectName("Copy_Button")
        self.gridLayout_3.addWidget(self.Copy_Button, 0, 0, 1, 3)
        self.Save_Button = QPushButtonStyleFirst(self._Button_Bar)
        self.Save_Button.setMinimumSize(QtCore.QSize(0, 30))
        self.Save_Button.setStyleSheet("background-color: rgb(0, 150, 135);")
        self.Save_Button.setObjectName("Save_Button")
        self.gridLayout_3.addWidget(self.Save_Button, 1, 1, 1, 2)
        self.gridLayout_4.addWidget(self._Button_Bar, 1, 2, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout_4, 1, 0, 1, 1)
        self.RedactorWidget.addWidget(self.Meme_Page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.RedactorWidget.addWidget(self.page_2)
        self.gridLayout_2.addWidget(self.RedactorWidget, 0, 0, 1, 1)
        self.Main.addWidget(self.Redactor_Page)
        self.gridLayout.addWidget(self.Main, 1, 1, 1, 1)

        self.retranslateUi(MemesLab)
        self.Main.setCurrentIndex(1)
        self.RedactorWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MemesLab)

    def retranslateUi(self, MemesLab):
        _translate = QtCore.QCoreApplication.translate
        MemesLab.setWindowTitle(_translate("MemesLab", "MemesLab"))
        self.Search_String.setPlaceholderText(_translate("MemesLab", "Введите \"#тег\" или \"описание\""))
        self.Search_Button.setText(_translate("MemesLab", "Поиск"))
        self._Text_MemesLab.setText(_translate("MemesLab", "MemesLab"))
        self.Home_Button.setText(_translate("MemesLab", " Домой           "))
        self.Add_Button.setText(_translate("MemesLab", " Редактор       "))
        self.Time_Lapse_Button.setText(_translate("MemesLab", " Time-Lapse    "))
        self.List_Button.setText(_translate("MemesLab", " Списки           "))
        self.Differed_Button.setText(_translate("MemesLab", " Отложенные "))
        self.Settings_Button.setText(_translate("MemesLab", " Настройки   "))
        self.label.setText(_translate("MemesLab", "<html><head/><body><p align=\"center\"><span style=\" font-size:36pt;\">Добро пожаловать в MemesLab!</span></p></body></html>"))
        self.From_Folder.setText(_translate("MemesLab", "Добавить файл через проводник"))
        self.List_Title.setText(_translate("MemesLab", "Для начала воспользуйтесь \"Поиск\""))
        self.Popular_Sorted.setText(_translate("MemesLab", "Популярное"))
        self.New_Sorted.setText(_translate("MemesLab", "Свежее"))
        self.Old_Sorted.setText(_translate("MemesLab", "Старое"))
        self.Meme_Title.setPlaceholderText(_translate("MemesLab", "Заголовок"))
        self.Meme_Redacting.setItemText(0, _translate("MemesLab", "Редактирование"))
        self.Meme_Redacting.setItemText(1, _translate("MemesLab", "Просмотр"))
        self.Change_Picture.setText(_translate("MemesLab", "Редактор картинки"))
        self.Meme_Group_String.setPlaceholderText(_translate("MemesLab", "Группа"))
        self.Meme_Description_String.setDocumentTitle(_translate("MemesLab", "Описание"))
        self.Meme_Description_String.setPlaceholderText(_translate("MemesLab", "Описание"))
        self.label_3.setText(_translate("MemesLab", "#"))
        self.Meme_Tag_String.setPlaceholderText(_translate("MemesLab", "Тег"))
        self.Meme_Add_Tag.setText(_translate("MemesLab", "Добавить"))
        self.Delete_Button.setText(_translate("MemesLab", "Удалить"))
        self.Copy_Button.setText(_translate("MemesLab", "Копировать"))
        self.Save_Button.setText(_translate("MemesLab", "Сохранить"))
from Source.QCastomWiget import QComboBoxCompleter, QLabelClickedStyleFirst, QLabelHighLight, QLabelUnderLine, QLineEditCompleter, QPushButtonMenu, QPushButtonStyleFirst, QScrollAreaHorizontalTag, QScrollAreaSpace, QWidgetPictureScaled
