# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Window.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Window_Round(object):
    def setupUi(self, Window_Round):
        Window_Round.setObjectName("Window_Round")
        Window_Round.resize(640, 480)
        Window_Round.setStyleSheet("background-color: rgb(24, 25, 29);\n"
"color: rgb(229, 229, 229);")
        self.Window = QtWidgets.QWidget(Window_Round)
        self.Window.setStyleSheet("")
        self.Window.setObjectName("Window")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.Window)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.Top = QtWidgets.QWidget(self.Window)
        self.Top.setMinimumSize(QtCore.QSize(0, 35))
        self.Top.setMaximumSize(QtCore.QSize(16777215, 35))
        self.Top.setStyleSheet("border-radius: 4px;")
        self.Top.setObjectName("Top")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.Top)
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.horizontalLayout.setContentsMargins(10, 5, -1, 5)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Title = QtWidgets.QLabel(self.Top)
        self.Title.setStyleSheet("font: 87 10pt \"Arial\";")
        self.Title.setText("")
        self.Title.setObjectName("Title")
        self.horizontalLayout.addWidget(self.Title)
        self.Roll = QLabelClicked(self.Top)
        self.Roll.setMinimumSize(QtCore.QSize(25, 25))
        self.Roll.setMaximumSize(QtCore.QSize(25, 25))
        self.Roll.setStyleSheet("QLabel:pressed { background-color: rgb(35, 35, 36);}\n"
"QLabel:hover { background-color: rgb(40, 46, 51);}")
        self.Roll.setText("")
        self.Roll.setPixmap(QtGui.QPixmap(":/top/Roll.png"))
        self.Roll.setScaledContents(True)
        self.Roll.setObjectName("Roll")
        self.horizontalLayout.addWidget(self.Roll)
        self.Fullscreen = QLabelClicked(self.Top)
        self.Fullscreen.setMinimumSize(QtCore.QSize(25, 25))
        self.Fullscreen.setMaximumSize(QtCore.QSize(25, 25))
        self.Fullscreen.setStyleSheet("QLabel:pressed { background-color: rgb(35, 35, 36);}\n"
"QLabel:hover { background-color: rgb(40, 46, 51);}")
        self.Fullscreen.setText("")
        self.Fullscreen.setPixmap(QtGui.QPixmap(":/top/Fullscreen.png"))
        self.Fullscreen.setScaledContents(True)
        self.Fullscreen.setObjectName("Fullscreen")
        self.horizontalLayout.addWidget(self.Fullscreen)
        self.Close = QLabelClicked(self.Top)
        self.Close.setMinimumSize(QtCore.QSize(25, 25))
        self.Close.setMaximumSize(QtCore.QSize(25, 25))
        self.Close.setStyleSheet("QLabel:pressed { background-color: rgb(80, 22, 22)}\n"
"QLabel:hover { background-color: rgb(155,0,0);}")
        self.Close.setText("")
        self.Close.setTextFormat(QtCore.Qt.PlainText)
        self.Close.setPixmap(QtGui.QPixmap(":/top/Close.png"))
        self.Close.setScaledContents(True)
        self.Close.setAlignment(QtCore.Qt.AlignCenter)
        self.Close.setObjectName("Close")
        self.horizontalLayout.addWidget(self.Close)
        self.verticalLayout.addWidget(self.Top)
        self.centralwidget = QtWidgets.QWidget(self.Window)
        self.centralwidget.setMinimumSize(QtCore.QSize(50, 50))
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout.addWidget(self.centralwidget)
        Window_Round.setCentralWidget(self.Window)

        self.retranslateUi(Window_Round)
        QtCore.QMetaObject.connectSlotsByName(Window_Round)

    def retranslateUi(self, Window_Round):
        _translate = QtCore.QCoreApplication.translate
        Window_Round.setWindowTitle(_translate("Window_Round", "MainWindow"))
from Source.QCastomWiget import QLabelClicked
