# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ContentForm.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ContentForm(object):
    def setupUi(self, ContentForm):
        ContentForm.setObjectName("ContentForm")
        ContentForm.resize(320, 0)
        ContentForm.setMinimumSize(QtCore.QSize(320, 0))
        ContentForm.setMaximumSize(QtCore.QSize(320, 16777215))
        ContentForm.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        ContentForm.setLayoutDirection(QtCore.Qt.LeftToRight)
        ContentForm.setAutoFillBackground(False)
        ContentForm.setStyleSheet("background-color: rgb(24, 25, 29);")
        self.verticalLayout = QtWidgets.QVBoxLayout(ContentForm)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.Picture = QWidgetPictureScaled(ContentForm)
        self.Picture.setMinimumSize(QtCore.QSize(320, 50))
        self.Picture.setObjectName("Picture")
        self.verticalLayout.addWidget(self.Picture)
        self.widget = QtWidgets.QWidget(ContentForm)
        self.widget.setMinimumSize(QtCore.QSize(300, 40))
        self.widget.setMaximumSize(QtCore.QSize(16777215, 40))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 15, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Title = QLabelEnd3Point(self.widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Title.sizePolicy().hasHeightForWidth())
        self.Title.setSizePolicy(sizePolicy)
        self.Title.setStyleSheet("color: rgb(229, 229, 229);\n"
"font: 87 14pt \"Arial\";")
        self.Title.setWordWrap(True)
        self.Title.setObjectName("Title")
        self.horizontalLayout.addWidget(self.Title)
        self.BuferCopy = QLabelClickedStyleFirst(self.widget)
        self.BuferCopy.setMaximumSize(QtCore.QSize(32, 32))
        self.BuferCopy.setText("")
        self.BuferCopy.setPixmap(QtGui.QPixmap(":/icons/BuferCopy.png"))
        self.BuferCopy.setScaledContents(True)
        self.BuferCopy.setObjectName("BuferCopy")
        self.horizontalLayout.addWidget(self.BuferCopy)
        self.verticalLayout.addWidget(self.widget)
        self.scrollArea = QScrollAreaHorizontalTag(ContentForm)
        self.scrollArea.setMinimumSize(QtCore.QSize(0, 40))
        self.scrollArea.setMaximumSize(QtCore.QSize(16777215, 40))
        self.scrollArea.setStyleSheet("color: rgb(229, 229, 229);\n"
"border-radius: 8px;\n"
"font: 87 12pt \"Arial\";\n"
"")
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.verticalLayout.addWidget(self.scrollArea)

        self.retranslateUi(ContentForm)
        QtCore.QMetaObject.connectSlotsByName(ContentForm)

    def retranslateUi(self, ContentForm):
        _translate = QtCore.QCoreApplication.translate
        ContentForm.setWindowTitle(_translate("ContentForm", "ContentForm"))
        self.Title.setText(_translate("ContentForm", "TextLabel "))
from Source.QCastomWiget import QLabelClickedStyleFirst, QLabelEnd3Point, QScrollAreaHorizontalTag, QWidgetPictureScaled
