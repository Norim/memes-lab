# *- coding: utf-8 -*-
"""Класс окна настроект программы."""

import os
import json

from PyQt5.QtCore import Qt, QFile, QIODevice
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QCheckBox, QDesktopWidget, QSpacerItem, QSizePolicy, QComboBox, QLabel


class Setting(QWidget):
    def __new__(cls, parent=None):
        """Класс является синглетоном."""
        if not hasattr(cls, 'instance'):
            cls.instance = super(Setting, cls).__new__(cls)
        return cls.instance

    def __init__(self, parent=None):
        """Инициализация класса."""
        super().__init__()
        self.parent = parent

        self.setObjectName("Setting")
        self.setWindowTitle("Настройки")
        self.resize(300, 150)
        self.center()

        # self.main_window = None
        # self.dnd_widget = None
        # self.redactor = None

        self.themes_list = ["Dark", "Light", "Razer"]

        # Default value:
        self.theme = "Dark"
        self.allow_tray = True
        self.allow_widget = True

        self.path_dark = ":/Themes/dark_theme.qss"
        self.path_light = ":/Themes/light_theme.qss"

        self.__load()
        self.__setupUi()

    @property
    def main_window(self):
        return self.parent

    @property
    def dnd_widget(self):
        return self.parent.dnd_widget

    @property
    def redactor(self):
        return self.parent.redactor

    def refresh(self):
        """Метод обновления окна и параметров программы."""
        self.allow_tray = self.checkBox_tray.isChecked()
        self.allow_widget = self.checkBox_widget.isChecked()

        self.main_window.refresh()
        self.dnd_widget.refresh()

    def update_styles(self):
        self.theme = self.themes_box.currentText()

        if self.theme == "Dark":
            file = QFile(self.path_dark)
            file.open(QIODevice.ReadOnly)
            style = file.readAll().data().decode("latin1")
            self.setStyleSheet(style)
            self.redactor.setStyleSheet(style)
        elif self.theme == "Light":
            file = QFile(self.path_light)
            file.open(QIODevice.ReadOnly)
            style = file.readAll().data().decode("latin1")
            self.setStyleSheet(style)
            self.redactor.setStyleSheet(style)
        elif self.theme == "Razer":
            pass

    def center(self):
        """Метод для установки окна по центру экрана."""
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def showEvent(self, e):
        """Переопределение события показа окна."""
        self.main_window.show()
        e.accept()

    def closeEvent(self, e):
        e.accept()
        self.__save()

    def __load(self):
        if not os.path.exists("Setting.json"):
            self.__save()
        else:
            with open("Setting.json", "r") as f:
                param = json.load(f)
                self.allow_tray = param[0]
                self.allow_widget = param[1]
                self.theme = param[2]

    def __save(self):
        param = [self.allow_tray, self.allow_widget, self.theme]
        with open("Setting.json", "w") as f:
            json.dump(param, f)

    def __setupUi(self):
        """Метод конструирования окна настроек."""
        self.setWindowFlags(Qt.CustomizeWindowHint |
                            Qt.WindowTitleHint |
                            Qt.WindowCloseButtonHint |
                            Qt.WindowShadeButtonHint |
                            Qt.Tool |
                            Qt.WindowStaysOnTopHint)

        self.checkBox_widget = QCheckBox("Включить виджет")
        self.checkBox_widget.setChecked(self.allow_widget)
        self.checkBox_widget.stateChanged.connect(self.refresh)

        self.checkBox_tray = QCheckBox("Включить иконку в трее")
        self.checkBox_tray.setChecked(self.allow_tray)
        self.checkBox_tray.stateChanged.connect(self.refresh)

        self.label_themes = QLabel("Выбор цветовой темы:")
        self.themes_box = QComboBox()
        self.themes_box.addItems(self.themes_list)
        self.themes_box.setCurrentText(self.theme)
        self.themes_box.currentTextChanged.connect(self.update_styles)

        self.VLayout = QVBoxLayout()
        self.VLayout.addWidget(self.checkBox_widget)
        self.VLayout.addWidget(self.checkBox_tray)

        self.VLayout.addWidget(self.label_themes)
        self.VLayout.addWidget(self.themes_box)
        self.VLayout.addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.MinimumExpanding))
        self.setLayout(self.VLayout)
