var searchData=
[
  ['image_5fundo_554',['image_undo',['../class_source_1_1cls__mempaint_1_1_mem_paint.html#aeac45fe93967c614ec8dbe905e90c587',1,'Source::cls_mempaint::MemPaint']]],
  ['increase_5fcopynumber_5fby_5fid_555',['increase_CopyNumber_by_Id',['../class_source_1_1cls__database_1_1_data_base_manager.html#a9eb62709b45ee520c0dcaa15f58ac9e4',1,'Source::cls_database::DataBaseManager']]],
  ['isarchive_556',['isArchive',['../namespace_source_1_1fun__extension.html#ab7b94f4f52de19555744e3a7e59e373a',1,'Source::fun_extension']]],
  ['isaudio_557',['isAudio',['../namespace_source_1_1fun__extension.html#a8b2e7938c6092d589a208a5c9734a35f',1,'Source::fun_extension']]],
  ['isbook_558',['isBook',['../namespace_source_1_1fun__extension.html#ad0e76261665b437f8e3ef3070e8e635b',1,'Source::fun_extension']]],
  ['isdocument_559',['isDocument',['../namespace_source_1_1fun__extension.html#a340e9ce3f47921c633c37bf0cd6de4bb',1,'Source::fun_extension']]],
  ['isimage_560',['isImage',['../namespace_source_1_1fun__extension.html#a7e623de30fabb31b691dc0e60f09d448',1,'Source::fun_extension']]],
  ['ismsexcel_561',['isMSExcel',['../namespace_source_1_1fun__extension.html#a8ccdd5cb1e652968ac66e347a0c42775',1,'Source::fun_extension']]],
  ['ismsoffice_562',['isMSOffice',['../namespace_source_1_1fun__extension.html#a6055cf83460a2a4341e9b569bd991e3e',1,'Source::fun_extension']]],
  ['ismspower_563',['isMSPower',['../namespace_source_1_1fun__extension.html#a84e6b5909ef1f5debaccdce9c0ab7852',1,'Source::fun_extension']]],
  ['ismsword_564',['isMSWord',['../namespace_source_1_1fun__extension.html#adf11864af77d19def64f8fddd583c6ae',1,'Source::fun_extension']]],
  ['isnonetype_565',['isNoneType',['../namespace_source_1_1fun__extension.html#a8f70408f316d398200d2c0aa4e8997c6',1,'Source::fun_extension']]],
  ['isvideo_566',['isVideo',['../namespace_source_1_1fun__extension.html#a4b19a18ee32e3e455bbc7d4b09b7d872',1,'Source::fun_extension']]]
];
