# *- coding: utf-8 -*-

path = "..\\..\\Source\\UI_MainWindow.py"
target = "import Resources_rc\n"
to_deleted = ["_to_deleted_", "self.centralwidget = QtWidgets.QWidget(MemesLab)",
            "MemesLab.setCentralWidget(self.centralwidget)"] #удалить ненужные элементы

f = open(path,"r")
lines = f.readlines()
f.close()
f = open(path,"w")
for line in lines:
    if line!=target:
        is_ok = True
        for dlt in to_deleted:
            if dlt in line:
                is_ok = False
        if is_ok:
            f.write(line)
f.close()

path = "..\\..\\Source\\UI_ContentForm.py"

to_deleted = "_to_deleted_"

f = open(path,"r")
lines = f.readlines()
f.close()
f = open(path,"w")
for line in lines:
    if line!=target:
        if not to_deleted in line:
            f.write(line)
    else:
        break
f.close()

path = "..\\..\\Source\\UI_RoundWindow.py"
target = "import Resources_rc\n"

f = open(path,"r")
lines = f.readlines()
f.close()
f = open(path,"w")
for line in lines:
    if line!=target:
        f.write(line)

f.close()
