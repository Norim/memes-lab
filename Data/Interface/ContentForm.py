# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ContentForm.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ContentForm(object):
    def setupUi(self, ContentForm):
        ContentForm.setObjectName("ContentForm")
        ContentForm.resize(350, 472)
        ContentForm.setMinimumSize(QtCore.QSize(350, 0))
        ContentForm.setMaximumSize(QtCore.QSize(360, 16777215))
        ContentForm.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        ContentForm.setLayoutDirection(QtCore.Qt.LeftToRight)
        ContentForm.setAutoFillBackground(False)
        ContentForm.setStyleSheet("background-color: rgb(24, 25, 29);")
        self.verticalLayout = QtWidgets.QVBoxLayout(ContentForm)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.Picture = QtWidgets.QLabel(ContentForm)
        self.Picture.setMaximumSize(QtCore.QSize(350, 350))
        self.Picture.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.Picture.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.Picture.setFrameShadow(QtWidgets.QFrame.Plain)
        self.Picture.setLineWidth(1)
        self.Picture.setText("")
        self.Picture.setPixmap(QtGui.QPixmap(":/icons/VoidPicture.png"))
        self.Picture.setScaledContents(True)
        self.Picture.setAlignment(QtCore.Qt.AlignCenter)
        self.Picture.setObjectName("Picture")
        self.verticalLayout.addWidget(self.Picture)
        self.widget = QtWidgets.QWidget(ContentForm)
        self.widget.setMinimumSize(QtCore.QSize(300, 60))
        self.widget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 15, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Title = QLabelEnd3Point(self.widget)
        self.Title.setStyleSheet("color: rgb(229, 229, 229);\n"
"font: 87 14pt \"Arial\";")
        self.Title.setWordWrap(True)
        self.Title.setObjectName("Title")
        self.horizontalLayout.addWidget(self.Title)
        self.BuferCopy = QLabelClicked(self.widget)
        self.BuferCopy.setMaximumSize(QtCore.QSize(32, 32))
        self.BuferCopy.setText("")
        self.BuferCopy.setPixmap(QtGui.QPixmap(":/icons/BuferCopy.png"))
        self.BuferCopy.setScaledContents(True)
        self.BuferCopy.setObjectName("BuferCopy")
        self.horizontalLayout.addWidget(self.BuferCopy)
        self.verticalLayout.addWidget(self.widget)
        self.scrollArea = QtWidgets.QScrollArea(ContentForm)
        self.scrollArea.setMaximumSize(QtCore.QSize(16777215, 55))
        self.scrollArea.setStyleSheet("color: rgb(229, 229, 229);\n"
"\n"
"font: 87 12pt \"Arial\";\n"
"")
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.Tag_layout = QtWidgets.QWidget()
        self.Tag_layout.setGeometry(QtCore.QRect(0, 0, 350, 55))
        self.Tag_layout.setObjectName("Tag_layout")
        self.formLayout = QtWidgets.QFormLayout(self.Tag_layout)
        self.formLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.formLayout.setContentsMargins(0, 2, 0, 15)
        self.formLayout.setObjectName("formLayout")
        self.label = QLabelTagToSearch(self.Tag_layout)
        self.label.setMinimumSize(QtCore.QSize(80, 30))
        self.label.setMaximumSize(QtCore.QSize(71, 30))
        self.label.setStyleSheet("background-color: rgb(0, 150, 135);\n"
"border-radius: 8px;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.scrollArea.setWidget(self.Tag_layout)
        self.verticalLayout.addWidget(self.scrollArea)

        self.retranslateUi(ContentForm)
        QtCore.QMetaObject.connectSlotsByName(ContentForm)

    def retranslateUi(self, ContentForm):
        _translate = QtCore.QCoreApplication.translate
        ContentForm.setWindowTitle(_translate("ContentForm", "ContentForm"))
        self.Title.setText(_translate("ContentForm", "TextLabel "))
        self.label.setText(_translate("ContentForm", "TextLabel"))
from Source.QCastomWiget import QLabelClicked, QLabelEnd3Point, QLabelTagToSearch
import Resources_rc
