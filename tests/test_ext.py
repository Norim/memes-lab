# *- coding: utf-8 -*-
import sys
import os
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
src_path = os.path.join(parentdir, 'Source')
sys.path.insert(0, parentdir)
sys.path.insert(0, src_path)

import Source.fun_extension as ext


def test_getExt():
    assert ext.getExt("disk\\dir\\file.py") == "py"
    assert ext.getExt("disk\\dir\\file.doc") == "doc"
    assert ext.getExt("disk\\dir\\file.docx") == "docx"
    assert ext.getExt("disk\\dir\\file.exe") == "exe"
    assert ext.getExt("disk\\dir\\file.") == ""
    assert ext.getExt("disk\\dir\\file") == ""


def test_isNoneType():
    assert ext.isNoneType("disk\\dir\\file") == True
    assert ext.isNoneType("disk\\dir\\file.") == True
    assert ext.isNoneType("disk\\dir\\file.  ") == True
    assert ext.isNoneType("disk\\dir\\file.doc") == False
    assert ext.isNoneType("disk\\dir\\file.exe") == False
    assert ext.isNoneType("disk\\dir\\file.png") == False
